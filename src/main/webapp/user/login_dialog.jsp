<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript"> 
	var loginDialog;
	var loginTabs;
	$(function() {
		if ('${LOGIN_USER_KEY.user.id}') {/*如果已经登陆显示用户信息*/
			$("#loginInfo").html('欢迎您：${LOGIN_USER_KEY.user.truename}') ;
		}
		
		loginTabs = $('#loginTabs').tabs({
			onSelect: function(title) {
				kaptcha();
			}
		});
	});
	
	function loginView(){
		loginDialog = $('#loginDialog').show().dialog({
			title: "&nbsp;登陆",
            width: 430, height: 250, top: 200,
            closable : false,
            iconCls: "ext_lock",
            modal: true,
			buttons : [ {
				text : '&nbsp;&nbsp;系统登录',
				iconCls : "icon-standard-key",
				handler : function() {
					loginFun();
				}
			} ]
		});
		var USER_ID = '${LOGIN_USER_KEY.user.id}';
		if (USER_ID) {/*如果已经登陆过了，那么刷新页面后也不需要弹出登录窗体*/
			loginDialog.dialog('close');
		} else {
			kaptcha();
		}
		$('form input').bind('keyup', function(event) {
			if (event.keyCode == '13') {
				loginFun();
			}
		});
	}
	
	function loginFun() {
		var k = $('#loginTabs').tabs('getSelected').find('form input[name=kaptcha]').val() ;
		tabsForm = $('#loginTabs').tabs('getSelected').find('form');//当前选中的tab
		if(tabsForm.form('validate') && "" != k) {
    		$.post($tools.basePath+"/sysmgr/userAction/doNotNeedSession_login.do", tabsForm.form("getData"), function(result) {
    			if (result.status) {
    				$("#loginInfo").html('欢迎您：'+(undefined != result.obj.user.truename?result.obj.user.truename:"")) ;
    				$("#loginDialog").dialog('close');
    				$(mainTab).tabs("getSelected").panel('refresh');
    			} else {
    				jrumble();
    				alertify.warning(result.msg);
    			}
    		}, 'json');
    	} else {
    		jrumble();
    		alertify.warning("验证码不能为空！");
    	}
	}
	
	function loginOut(b){
    	$.post($tools.basePath+"/sysmgr/userAction/doNotNeedSession_logout.do", null, function(result) {
    		if ("exit"==b) {
    			location.replace($tools.basePath+'/index.jsp');
    		} else {
    			$("#loginInfo").html('') ;
    			$('#loginDialog').dialog('open');
    			kaptcha();
    		}
    	}, 'json');
    };
    
    function kaptcha() {
    	var temp_img = new Image();
    	$(temp_img).bind('load', function () {
    		$("#kaptcha").attr("src", temp_img.src).click(function() {
    			$(this).attr('src',$tools.basePath+'/images/loading5.gif').fadeIn();
    			temp_img.src = $tools.basePath+'/common/page/image.jsp?' + Math.floor(Math.random() * 100) ;
    			$(temp_img).bind('load', function () {
    				$("#kaptcha").attr("src", temp_img.src) ;
    			});
    			
    		}) ;
		});
    	temp_img.src = $tools.basePath+'/common/page/image.jsp?' + Math.floor(Math.random() * 100) ;
    }
	//表单晃动
	function jrumble() {
		$('.inner').jrumble({
			x : 4,
			y : 0,
			rotation : 0
		});
		$('.inner').trigger('startRumble');
		setTimeout('$(".inner").trigger("stopRumble")', 500);
	}
</script>
<style>
#loginDialog .dialog-content {padding:0px;}
#loginDialog td{border:0px;}
</style>
<div id="loginDialog" class="inner" style="display: none;">
	<div id="loginTabs" class="easyui-tabs" data-options="fit:true,border:false">
		<div style="padding-top:15px;" data-options="title: '用户输入模式', refreshable: false,selected:true, iconCls:'icon-standard-textfield-key'">
			<form id="form-login" class="easyui-form">
				
			 	<table class="grid"> 
					<tr>
						<td style="width:90px;text-align:right;">账　　号：</td>
						<td><input name="account" value="admin" class="easyui-validatebox" style="width:250px;" type="text" data-options="required:true, prompt: '账号'" ></td>
					</tr>	
					<tr>
						<td style="width:90px;text-align:right;">密　　码：</td>
						<td><input name="password" value="123456" class="easyui-validatebox" style="width:250px;" type="text" data-options="required:true, prompt: '密码'" ></td>
					</tr>	
					<tr>
						<td style="width:90px;text-align:right;">验  证  码：</td> 
						<td>
							<input name="kaptcha" class="easyui-validatebox" style="float:left;width:120px;" type="text" >
							<img id="kaptcha" src="${pageContext.request.contextPath}/images/loading5.gif">
						</td>
					</tr>	
			 	</table>
			</form>
		</div>
	</div>
</div>