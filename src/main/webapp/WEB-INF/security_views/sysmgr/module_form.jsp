<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<script type="text/javascript">
	var form_url = $tools.basePath+"/sysmgr/moduleAction/add.do" ;
	
	var s1, s2 ;
	$(function() {
		s1 = $("#select1").combotree({
			url : $tools.basePath+"/sysmgr/moduleAction/doNotNeedAuth_combox.do",
			idFiled:'pid', textFiled:'name', editable: false,
			disabled: true, required:false, lines:true, autoShowPanel: false
	    }); 
		s2 = $("#select2").combobox({
			url : $tools.basePath+"/sysmgr/moduleAction/doNotNeedAuth_viewaction.do",
			valueField: 'id', textField: 'actionName',panelHeight:'auto', required:false, editable:false,
			onSelect: function(node){
				$("input[name=action_value]").val(node.actionValue);
			}
	    });
		$("#select3").combobox({
			valueField: 'label', textField: 'value', value: 'R',
			data: [{ label: 'R', value: '导航栏目' },{ label: 'F', value: '导航菜单' },{ label: 'O', value: '菜单操作' }],
			panelHeight:'auto', editable:false, autoShowPanel: true,
			onSelect: function(node){
				if("R" == node.label) {
					s1.combotree({ value: '', disabled: true }) ;
					s2.combobox({value: '', disabled: true, required: false});
					$("#mv").validatebox({required: false});
					$("#mv").attr("disabled", true) ;
					$("#url").validatebox({required: false});
					$("#url").attr("disabled",true) ;
				} else if("F" == node.label) {
					s1.combotree({disabled: false, required:true, autoShowPanel: true }) ;
					s2.combobox({disabled: true, required: false});
					$("#mv").validatebox({required: false});
					$("#mv").attr("disabled",true) ;
					$("#url").validatebox({required: false});
					$("#url").attr("disabled",false) ;
				} else if("O" == node.label) {
					s1.combotree({ disabled: false, required:true, autoShowPanel: true }) ;
					s2.combobox({ disabled: false, required: true});
					$("#mv").validatebox({required: true});
					$("#mv").attr("disabled",false) ;
					$("#url").validatebox({required: true});
					$("#url").attr("disabled",false) ;
				}
			}
	    });
		$("#select4").combobox({
			valueField: 'label', textField: 'value', value: 'Y',
			data: [{ label: 'Y', value: '启用' },{ label: 'N', value: '禁用' }],
			panelHeight:'auto', editable:false, autoShowPanel: true
	    });
		
		
		//编辑，加载表单数据
		if($('input[name=id]').val().length > 0) {
			form_url = $tools.basePath+"/sysmgr/moduleAction/edit.do" ;
			$.post($tools.basePath+"/sysmgr/moduleAction/get.do", {id:$('input[name=id]').val()}, function(result) {
				if (result.id != undefined) {
					$('form').form('load', {
						'id' : result.id,
						'moduleName' : result.moduleName,
						'linkUrl' : result.linkUrl,
						'action_id' : result.action_id,
						'action_value' : result.action_value,
						'permit_id' : result.permit_id,
						'disused' : result.disused,
						'type' : result.type,
						'iconCls' : result.iconCls,
						'pid' : result.pid
					});
					
					if("R" == result.type) {
						s1.combotree({ value: '', disabled: true }) ;
						s2.combobox({value: '', disabled: true, required: false});
						$("#mv").validatebox({required: false});
						$("#mv").attr("disabled", true) ;
						$("#url").validatebox({required: false});
						$("#url").attr("disabled",true) ;
					} else if("F" == result.type) {
						s1.combotree({ disabled: false, required:true, value: result.pid }) ;
						s2.combobox({disabled: true, required: false});
						$("#mv").validatebox({required: false});
						$("#mv").attr("disabled",true) ;
						$("#url").validatebox({required: false});
						$("#url").attr("disabled",false) ;
					} else if("O" == result.type) {
						s1.combotree({ disabled: false, required:true, value: result.pid }) ;
						s2.combobox({ disabled: false, required: true});
						$("#mv").validatebox({required: true});
						$("#mv").attr("disabled",false) ;
						$("#url").validatebox({required: true});
						$("#url").attr("disabled",false) ;
					}
				}
			}, 'json');
		} else {
			s1.combotree({ disabled: true, required:false, autoShowPanel: true }) ;
			s2.combobox({value: '', disabled: true});
		}
		$("#mv").validatebox({required: false});
		$("#mv").attr("disabled", true) ;
		$("#url").attr("disabled", true) ;
		s2.combobox({disabled: true});
	});
	
	//提交表单数据
	var submitNow = function($d, $tg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				$tg.treegrid('reload') ;
				window.mainpage.refreshNavTab();
				alertify.success(result.msg);$.easyui.loaded(); $d.dialog("close") ;
			} else {
				$.easyui.loaded();alertify.error(result.msg);
			}
		}, 'json').error(function(){$.easyui.loaded();});
	};
	
	//验证表单
	var submitForm = function($d, $tg) {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $tg) ; ;
		}
	};
	
	
</script>
<div class="dialog_centent">
	<form id="form" class="easyui-form">
		<input type="hidden" name="id" value="${id}" />
		<input type="hidden" name="permit_id" />
		<table class="grid">
			<tr>
				<td>名称：</td>
				<td><input name="moduleName" class="easyui-validatebox" type="text" data-options="required: true, prompt: '菜单名称'" /></td>
				<td>类型：</td>
				<td><select id="select3" name="type" style="width:218px;height:30px;"></select></td>
			</tr>
			<tr>
				<td>权限值：</td>
				<td><input id="mv" name="moduleValue" class="easyui-validatebox" type="text" /></td>
				<td>动作：</td>
				<td>
					<input type="hidden" name="action_value">
					<select id="select2" name="action_id" style="width:218px;height:30px;"></select>
				</td>
			</tr>
			<tr>
				<td>URL地址：</td>
				<td colspan="3"><input id="url" name="linkUrl" class="easyui-validatebox text1" type="text" data-options="required: false, prompt: '菜单地址'" /></td>
			</tr>
			<tr>
				<td>父模块：</td>
				<td><select id="select1" name="pid" style="width:218px;height:30px;"></select><a onClick="s1.combotree('setValue','');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_clear'"></a></td>
				<td>是否启用：</td>
				<td><select id="select4" name="disused" style="width:218px;height:30px;"></select></td>
			</tr>
			<tr>
				<td>排序：</td>
				<td><input name="seq" class="easyui-numberspinner" type="text" data-options="min:1,value:1" style="width:218px;height:30px;" /></td>
				<td>图标：</td>
				<td><select id="iconCls" name="iconCls" class="easyui-comboicons" data-options="autoShowPanel: false, multiple: false, size: '16'" style="width:218px;height:30px;"></select></td>
			</tr>
		</table>
	</form>
</div>
