<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript">
	var form_url = $tools.basePath+"/sysmgr/userAction/add.do" ;
	var tabsContainer ;
	$(function() {
		tabsContainer = $("#tabsContainer").tabs({
			fit: true, border: false, showOption: true, enableNewTabMenu: true, tabPosition: 'left', headerWidth: 120
		}) ;
		/* 
		$.each(tabsContainer.tabs("tabs"), function(i, p) {
			if(i>0) {
				tabsContainer.tabs("disableTab", i);
			}
		});
		 */
		 
		$("#sex").combobox({
			data: [{ label: '男', value: '男' },{ label: '女', value: '女' }],
			valueField:'label', textField:'value', editable: false, value: "男",
			required:false, lines:true, autoShowPanel: false, panelHeight:'auto'
   		}); 
		
		//编辑，加载表单数据
		if($('input[name=id]').val().length > 0) {
			form_url = $tools.basePath+"/sysmgr/userAction/edit.do" ;
			$.post($tools.basePath+"/sysmgr/userAction/get.do", {id:$('input[name=id]').val()}, function(result) {
				if (result.id != undefined) {
					$('form').form('load', {
						'id' : result.id,
						'account' : result.account,
						'password' : result.password,
						'status' : result.status,
						'truename' : result.truename,
						'sex' : result.sex,
						'email' : result.email,
						'dept_id' : result.dept_id
					});
				}
			}, 'json').error(function(){$.easyui.loaded();});;
		}
	});
	
	//保存表单并继续
	var apply = function() {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据正在保存，请稍等..." });
			$.post(form_url, $("#form").form("getData"), function(result) {
				if (result.status) {
					$dg.datagrid('reload') ;
					$.easyui.loaded();
				} else {
					alertify.error(result.msg);
				}
			}, 'json').error(function(){$.easyui.loaded();});
		}
	};
	
	//提交表单数据
	var submitNow = function($d, $dg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				$dg.datagrid('reload') ;
				alertify.success(result.msg);
				$.easyui.loaded(); $d.dialog("close") ;
			} else {
				alertify.error(result.msg);
			}
		}, 'json');
	};
	
	//验证表单
	var submitForm = function($d, $dg) {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $dg) ;
		} 
	};
</script>
<style type="text/css">
    .panel-body  { overflow-y: auto; }
</style>


<div class="easyui-layout" data-options="fit: true">

	<form id="form" class="easyui-form">
	<input type="hidden" name="id" value="${id}" />
	
	<div data-options="region: 'center', border: true" style="border-left:0px;border-top:0px;border-bottom:0px;overflow: hidden;">
		
		<div id="tabsContainer">
	        <div style="padding:2px;" class="panel-container" data-options="title: '基本信息', iconCls: 'icon-standard-layout-header', refreshable: false, selected: true">
	        	<div class="dialog_centent">
				<table class="grid">
					<tr>
						<td>姓　　名：</td>
						<td><input name="name" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>员工编号：</td>
						<td><input name="emp_no" class="easyui-validatebox" type="text" data-options="" /></td>
						<td rowspan="17" style="width:170px;" valign="top">
							<p style="width:110px;height:150px;border:1px solid #ccc;margin:5px auto;"></p>
							<p style="margin:0 auto ;text-align: center;">
								<a onClick="" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_add'">选择照片</a>
							</p>
						</td>
					</tr>
					<tr>
						<td>用户帐号：</td>
						<td><input name="account" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>账号状态：</td>
						<td>
							<select class="easyui-combobox" style="width:218px;height:30px;" name="status" data-options="
								valueField: 'label', textField: 'value', editable: false, value : '0',
								data: [{ label: '0', value: '激活' },{ label: '1', value: '禁用' }],
								panelHeight:'auto', editable:false"></select>
						</td>
					</tr>
					<tr>
						<td>隶属部门：</td>
						<td></td>
						<td>籍　　贯：</td>
						<td><input name="origin" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>工资卡号：</td>
						<td><input name="bank_card" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>社&nbsp;&nbsp;保&nbsp;&nbsp;号：</td>
						<td><input name="txtTBNo" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>曾&nbsp;&nbsp;用&nbsp;&nbsp;名：</td>
						<td><input name="old_name" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>性　　别：</td>
						<td>
							<select id="sex" style="width:218px;height:30px;" name="sex"></select>
						</td>
					</tr>
					<tr>
						<td>出生日期：</td>
						<td><input name="birth" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>年　　龄：</td>
						<td><input name="age" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>出&nbsp;&nbsp;生&nbsp;&nbsp;地：</td>
						<td><input name="home_place" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>民　　族：</td>
						<td><input name="folk" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>户　　口：</td>
						<td><input name="rpr" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>户口性质：</td>
						<td><input name="ddlHKXZ" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>婚姻状况：</td>
						<td><input name="ddlMarriage" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>员工身份：</td>
						<td><input name="ddlYGSF" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>国　　籍：</td>
						<td><input name="txtNation" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>证件类型：</td>
						<td><input name="ddIDCardType" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>政治面貌：</td>
						<td><input name="ddlPlo" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>健康状况：</td>
						<td><input name="ddlHealth" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>身　　高：</td>
						<td><input name="txtStature" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>体　　重：</td>
						<td><input name="txtWeight" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>视　　力：</td>
						<td><input name="txtSight" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>最高学历：</td>
						<td><input name="ddlEducation" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>最高学位：</td>
						<td><input name="ddlDegree" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>专　　业：</td>
						<td><input name="txtSpeciality" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
					<tr>
						<td>职　　称：</td>
						<td><input name="ddlTechnicalTitle" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>毕业院校：</td>
						<td><input name="txtSchool" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr> 
					<tr>
						<td>参加工作日期：</td>
						<td><input name="ToWork" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>空闲工龄：</td>
						<td>
							<input name="txtFreeLen1" style="width:80px;" class="easyui-validatebox" type="text" data-options="" />月
							<input name="txtYOS" style="width:80px;" class="easyui-validatebox" type="text" data-options="" />年
						</td>
					</tr>
					<tr>
						<td>创 建 人：</td>
						<td><input name="txtCreator" class="easyui-validatebox" type="text" data-options="" /></td>
						<td>创建时间：</td>
						<td><input name="txtCreateTime" class="easyui-validatebox" type="text" data-options="" /></td>
					</tr>
				</table>
				</div>
				
				<div class="dialog_centent">
				<div id="tabsContainer" style="margin-top:2px;" class="easyui-tabs" data-options="border: false, showOption: true, enableNewTabMenu: true, tabPosition: 'top'">
			        <div class="panel-container" data-options="title: '职位信息',refreshable: false, iconCls: 'icon-standard-layout-header', selected: true" style="position: relative;">
			        	<div class="form_base">
							<table>
								<tr>
									<td>创建人：</td>
									<td><input name="" class="easyui-validatebox" type="text" data-options="" /></td>
									<td>创建时间：</td>
									<td><input name="" class="easyui-validatebox" type="text" data-options="" /></td>
								</tr>
							</table>
						</div>
			        </div>
		        </div>
				<div id="tabsContainer" class="easyui-tabs" data-options="border: false, showOption: true, enableNewTabMenu: true, tabPosition: 'top', headerWidth: 100">
			        <div class="panel-container" data-options="title: '联系方式',refreshable: false, iconCls: 'icon-standard-layout-header', selected: true" style="position: relative;">
			        	<div class="form_base">
							<table>
								<tr>
									<td>创建人：</td>
									<td><input name="" class="easyui-validatebox" type="text" data-options="" /></td>
									<td>创建时间：</td>
									<td><input name="" class="easyui-validatebox" type="text" data-options="" /></td>
								</tr>
							</table>
						</div>
			        </div>
		        </div>
				</div>
	        </div>
	        <div id="code" class="panel-container" data-options="disabled:true,title: '教育背景', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '外语水平', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '资格资历', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '工作经历', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '社会关系', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '健康信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '奖惩信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '任职信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '调动信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '面谈管理', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '酬薪定级', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '调薪记录', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '培训经历', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '考核成绩', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '工伤管理', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '社保公积金', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '合同信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	        <div id="code" class="panel-container" data-options="title: '薪资信息', iconCls: 'icon-standard-layout-header', refreshable: false">
	        </div>
	    </div>
	    
	</div>
	
	<div style="width:200px;" data-options="region: 'east', title: '人员列表', border: false" style="overflow: hidden;">
	
	</div>
	
	</form>
</div>
