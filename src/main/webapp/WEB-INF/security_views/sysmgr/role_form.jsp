<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<script type="text/javascript">
	var form_url = $tools.basePath+"/sysmgr/roleAction/add.do" ;
	
	$(function() {
		//编辑，加载表单数据
		if($('input[name=id]').val().length > 0) {
			form_url = $tools.basePath+"/sysmgr/roleAction/edit.do" ;
			$.post($tools.basePath+"/sysmgr/roleAction/get.do", {id:$('input[name=id]').val()}, function(result) {
				if (result.id != undefined) {
					$('form').form('load', {
						'id' : result.id,
						'name' : result.name,
						'description' : result.description,
						'pid' : result.pid
					});
				}
			}, 'json');
		}
	});
	
	//提交表单数据
	var submitNow = function($d, $dg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				$dg.datagrid("reload") ;
				alertify.success(result.msg);$.easyui.loaded(); $d.dialog("close") ;
			} else {
				alertify.error(result.msg);$.easyui.loaded();
			}
		}, 'json').error(function(){$.easyui.loaded();});
	};
	
	//验证表单
	var submitForm = function($d, $dg) {
		if($('#form').form("validate")) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $dg) ;
		}
	};
	
	
</script>
<div class="dialog_centent">
	<form id="form" class="easyui-form">
		<input type="hidden" name="id" value="${id}" />
		<table class="grid">
			<tr>
				<td>角色名称：</td>
				<td><input name="name" class="easyui-validatebox" type="text" data-options="required: true, prompt: '角色名称'" /></td>
			</tr>
			<tr>
				<td>描述：</td>
				<td><textarea name="description" style="height:70px;" class="easyui-validatebox" data-options="prompt: '角色备注'"></textarea></td>
			</tr>
		</table>
	</form>
</div>
