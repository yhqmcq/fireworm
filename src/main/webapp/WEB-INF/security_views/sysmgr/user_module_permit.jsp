<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>角色管理</title>
<%@ include file="/common/header/meta.jsp"%>
<%@ include file="/common/header/script.jsp"%>

<script type="text/javascript">
	var $dg ;
	var $tg ;
	$(function() {
		$dg = $("#d1").datagrid({
			url: $tools.basePath+"/sysmgr/userAction/datagrid.do",
			title: '权限资源授权', method: "post", idField: 'id', fit: true, border: false,
			remoteSort: false, toolbar: '#buttonbar', striped:true, pagination: true, singleSelect: true,
			frozenColumns: [[
			    { field: 'ck', checkbox: true },
			    { field: 'id', title: 'ID', width: 80, sortable: true }
			]],
			columns: [[
			    { field: 'account', title: '账号', width: 100, sortable: true },
			    { field: 'truename', title: '姓名', width: 100, sortable: true },
			    { field: 'showPermission', title: '关联', width: 40, align: 'center', formatter: function(value, row, index) {
				    return $.string.format('<span style="cursor: pointer;" onclick="get_module_permit(\'{0}\')"><img src="{1}" title="浏览"/></span>',row.id ,$tools.basePath+'/images/icons/view.png') ;
				}}
			]],
			enableHeaderClickMenu: true, enableHeaderContextMenu: true, selectOnRowContextMenu: false, pagingMenu: { submenu: false }
	    });
		
		$tg = $("#t2").treegrid({
			title: '菜单模块', width: 900, height: 400, method: "get",
			url: $tools.basePath+"/sysmgr/moduleAction/treegrid.do",
			idField: 'id', treeField: 'moduleName', fit: true, border: false,
			remoteSort: false, toolbar: '#buttonbar2', striped:true,singleSelect:false,
			frozenColumns: [[
			    { field: 'ck', checkbox: true },
			    { field: 'id', title: 'ID(id)', width: 80, sortable: true }
			]],
			columns: [[
			    { field: 'moduleName', title: '名称', width: 220, sortable: true },
			    { field: 'seq', title: '排序', width: 50, sortable: true },
			    { field: 'type', title: '模块类型', width: 60, sortable: true, formatter:function(value,row){
			    	if(value == "R"){return "导航栏目";}else if(value == "F"){return "<font color='green'>导航菜单</font>";}else{return "<font color='orange'>菜单操作</font>";}
			    }},
			    { field: 'disused', title: '禁用', width: 50, sortable: true, formatter:function(value,row){
			    	if(value == "Y"){return "<font color='green'>启用</font>";}else{return "<font color='red'>禁用</font>";}
			    }},
			    { field: 'linkUrl', title: '链接地址', width: 250 },
			    { field: 'moduleValue', title: '权限值', width: 140 },
			    { field: 'created', title: '日期', width: 140, sortable: true }
			]],
			onClickRow:function(row){   
             	$tg.treegrid('cascadeCheck',{			//级联选择   
                    id:row.id, 							//节点ID   
                    deepCascade:true 					//深度级联   
               	});   
         	},
	    });
	});
	
	function savePermission() {
		var selections=$tg.treegrid('getSelections');
		var selectionUser=$dg.datagrid('getSelected');
		var checkedIds=[];
		$.each(selections,function(i,e){
			checkedIds.push(e.id);
		});
		console.info(checkedIds) ;
		console.info(selectionUser) ;
		if(selectionUser){
			$.ajax({
				url:$tools.basePath+"/sysmgr/userAction/set_module_permit.do",
				data: "id="+selectionUser.id+"&moduleIds="+(checkedIds.length==0?"":checkedIds),
				success: function(result){
					result = $.parseJSON(result);
					if (result.status) {
						alertify.success(result.msg);
					} else {
						alertify.warning(result.msg);
					}
				},
				error:function(){
					alertify.error(result.msg);
				}
			});
		}else{
			$.easyui.messager.show({ icon: "info", msg: "请选择用户！" });
		}
	}
	
	function get_module_permit(userid) {
		$.post($tools.basePath+"/sysmgr/userAction/get_module_permit.do", {
			id : userid
		}, function(result) {
			$tg.datagrid('unselectAll');
			if(result.moduleIds) {
				var ids = result.moduleIds.split(",") ;
				$.each(ids,function(i,e){
					$tg.treegrid('select',e);
				});
			} else {
				alertify.warning("该用户为分配资源权限！");
			}
		}, 'JSON').error(function() { });
	}
	
	
</script>

</head>

<body style="padding: 0px; margin: 0px;">
	<div class="easyui-layout" data-options="fit: true">
	
		<div data-options="region: 'west', border: false, split:true,minWidth: 450, maxWidth: 450" style="width:450px;overflow: hidden;">
			<div id="d1">
				<div id="buttonbar">
                    <a onclick="$dg.datagrid('reload');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_reload'">刷新</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext_save" plain="true" onclick="savePermission();">保存设置</a>
                </div>
			</div>
		</div>
		
		<div data-options="region: 'center', border: false" style="overflow: hidden;">
			<div id="t2">
				<div id="buttonbar2">
                    <a onclick="$tg.treegrid('reload');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_reload'">刷新</a>
                    <a onclick="$tg.treegrid('unselectAll');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_cancel'">取消选中</a>
                </div>
			</div>
		</div>
		
	</div>	
</body>
</html>