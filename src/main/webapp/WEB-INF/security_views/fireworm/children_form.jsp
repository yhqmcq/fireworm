<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<script type="text/javascript">
	var form_url = $tools.basePath+"/fireworm/childrenAction/add.do" ; 
	
	var tabs, t, t1, s1, uploader;
	$(function() {
		var photo_area = $(".photo_area") ;
		tabs = $("#tabs").tabs({
            lineHeight: 0,
        }); 
		t1 = $("#t1").ueditor({
			width:748, height: 300, templet: 'simple', wordCount:false, 
			elementPathEnabled: false, scaleEnabled: true
		});
		
		var province = $("#province").combobox() ;
		var city = $("#city").combobox();
		var county = $("#county").combobox();
		province.combobox({
			url: $tools.basePath+"/sysmgr/areaAction/doNotNeedSession_getAreas.do",
			valueField: "id",
			textField: "areaName",
			onSelect: function(node) {
			 	$("#city").combobox("setValue",'');
	            $("#county").combobox("setValue",'');
	            var province = $('#province').combobox('getValue');     
	            if(province!=''){
	            	city.combobox({
	            		url: $tools.basePath+"/sysmgr/areaAction/doNotNeedSession_getAreas.do?id="+province,
	            		valueField: "id",
	        			textField: "areaName",
	        			onSelect: function(node) {
	        				$("#county").combobox("setValue",'');
	        				var city = $('#city').combobox('getValue');    
	        				 if(city!=''){
	        					 county.combobox({
        						 	url: $tools.basePath+"/sysmgr/areaAction/doNotNeedSession_getAreas.do?id="+city,
        		            		valueField: "id",
        		        			textField: "areaName",
        		        			onSelect: function(node) {
        		        				$('#county').combobox('setValue', node.areaName);
        		        			}
	        					 });
	        				 }
	        				 $('#city').combobox('setValue', node.areaName);
	        			}
	            	});
	            }
	            $('#province').combobox('setValue', node.areaName);
			}
			
		});
		
		$("#sex").combobox({
			data: [{ label: '男', value: '男' },{ label: '女', value: '女' }],
			valueField:'label', textField:'value', editable: false, value: "男",
			required:false, lines:true, autoShowPanel: false, panelHeight:'auto'
	    }); 
		$("#isPay").combobox({
			data: [{ label: '是', value: '是' },{ label: '否', value: '否' }],
			valueField:'label', textField:'value', editable: false, value: "否",
			required:false, lines:true, autoShowPanel: false, panelHeight:'auto',
			onSelect: function(node) {
				if("是" == node.value) {
					$("#money").validatebox({required: true});
					$("#money").attr("disabled",false) ;
				} else if("否" == node.value){
					$("#money").validatebox({required: false});
					$("#money").attr("disabled",true) ;
				}
			}
   		}); 
		$("#money").validatebox({required: false});
		$("#money").attr("disabled",true) ;
		
		//编辑，加载表单数据
		if($('input[name=id]').val().length > 0) {
			form_url = $tools.basePath+"/fireworm/childrenAction/edit.do" ;
			$.post($tools.basePath+"/fireworm/childrenAction/get.do", {id:$('input[name=id]').val()}, function(result) {
				if (result.id != undefined) {
					$('form').form('load', {
						'id' : result.id,
						'name' : result.name,
						'sname' : result.sname,
						'sex' : result.sex,
						'brith' : result.brith,
						'lost_time' : result.lost_time,
						'lost_height' : result.lost_height,
						'isPay' : result.isPay,
						'money' : result.money,
						'province' : result.province,
						'city' : result.city,
						'county' : result.county,
						'lost_addr' : result.lost_addr,
						'photos_path' : result.photos_path,
						'description' : result.description
					});
					
					if("是"==result.isPay) {
						$("#money").validatebox({required: true});
						$("#money").attr("disabled",false) ;
					}
				}
			}, 'json');
		}
		
		uploader = new plupload.Uploader({
			runtimes : 'html5,flash,html4',
			flash_swf_url : $tools.basePath+'/js/plugins/plupload-2.1.1/js/Moxie.swf',	//Flash支持
			url : $tools.basePath+'/upload?upload_path=/file_root/lost&pc=true&folderType=1&fileNameType=1',
			browse_button : 'pickfiles', 
			//chunk_size : '5mb',//把大文件分割  
			container: 'container', 
			init: {
				PostInit: function() {
				},
				FilesAdded: function(up, files) {
					uploader.start();
					$.easyui.loading({ msg: "图片上传中，请稍等..." });
				},
				FileUploaded: function(up, file, info) {
					var response = $.parseJSON(info.response);
					if (response.status) {
						
						$.post($tools.basePath+"/fireworm/childrenAction/doNotNeedAuth_resize_photo.do", {
							web_root: response.fileinfo.web_root,
							photos_path: response.fileinfo.saveDir+response.fileinfo.newName
						}, function(result) {
							if(result.status) {
								var photo_group = "" +
												  "<div class='pgroup'>"+
								 				  "<div class='p1'><img src='"+result.obj[1]+"'><p class='px1'>80×60</p></div>"+
												  "<div class='p2'><img src='"+result.obj[2]+"'><p class='px2'>150×100</p></div>"+
												  "<div class='p3'><img src='"+result.obj[3]+"'><p class='px3'>170×120</p></div>"+
											      "</div>";
								photo_area.append(photo_group) ;
								$.easyui.loaded();
								
								var photos_path = $("input[name=photos_path]") ;
								if(photos_path.val() != "") {
									photos_path.val(photos_path.val()+"|"+result.obj[0]) ;
								} else {
									photos_path.val(result.obj[0]);
								}
							} else {
								$.easyui.loaded();alertify.error(result.msg);
							}
						}, 'json').error(function() {
							$.easyui.loaded();alertify.error(result.msg);
						});
					} else {
						$.easyui.loaded();alertify.error("文件上传失败："+response.msg);
					}
				},
				UploadProgress: function(up, file) {
				},
				Error: function(up, err) {
					$.easyui.loaded();alertify.error("错误："+ err.code+"---"+err.message+"---"(err.file ? ", File: " + err.file.name : ""));
					up.refresh(); 
				}
			}
		});
		uploader.init();
	});
	
	//提交表单数据
	var submitNow = function($d, $dg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				alertify.success(result.msg);$.easyui.loaded(); $dg.datagrid("reload");$d.dialog("close") ;
			} else {
				$.easyui.loaded();alertify.error(result.msg);
			}
		}, 'json').error(function(){$.easyui.loaded();});
	};
	
	//验证表单
	var submitForm = function($d, $dg) {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $dg) ; ;
		} 
	};
	
	
</script>

<form id="form" class="easyui-form">
	<input type="hidden" name="id" value="${id}" />
	<div id="tabs" style="margin:2px;">
        <div data-options="title: '职位信息',refreshable: false, iconCls: 'icon-standard-layout-header'">
        	<div class="dialog_centent">
	        	<table class="grid" style="border:0px;margin-bottom: 5px;">
					<tr>
						<td>名称：</td>
						<td><input name="name" class="easyui-validatebox" type="text" data-options="required: true" /></td>
						<td>小名：</td>
						<td><input name="sname" class="easyui-validatebox" type="text" /></td>
					</tr>
					<tr>
						<td>性别：</td>
						<td>
							<select id="sex" style="width:218px;height:30px;" name="sex" ></select>
						</td>
						<td>出生日期：</td> 
						<td><input name="brith" style="height:30px;width:218px;" class="easyui-my97" type="text" readonly="readonly" data-options="required: true, maxDate:'new Date()',isShowClear:false" /></td>
					</tr>
					<tr>
						<td>丢失时间：</td>
						<td><input name="lost_time" style="height:30px;width:218px;" class="easyui-my97" type="text" readonly="readonly" datefmt="yyyy-MM-dd HH:mm:ss" data-options="required: true,isShowClear:false" /></td>
						<td>丢前升高：</td>
						<td><input name="lost_height" style="height:30px;width:218px;" class="easyui-numberspinner" type="text" data-options="required: true,missingMessage:'该输入项为必须输项'" /> CM</td>
					</tr>
					<tr>
						<td>是否酬谢：</td>
						<td>
							<select id="isPay" style="width:218px;height:30px;" name="isPay"></select>
						</td>
						<td>酬谢金额：</td>
						<td><input id="money" name="money" style="height:30px;" class="easyui-validatebox" value="0.0" type="text" /></td>
					</tr>
					<tr>
						<td>丢失地点：</td>
						<td colspan="3">
							省<select style="width:120px;height:30px;" id="province" name="province"></select>
							市<select style="width:120px;height:30px;" id="city" name="city"></select>
							县<select style="width:120px;height:30px;" id="county" name="county"></select>
							<input name="lost_addr" style="width:168px;" class="easyui-validatebox" type="text" />
						</td>
					</tr>
				</table>
				<div id="t1" name="description" ></div>
			</div>
		</div>
		<style>
			.photo_area{ 
				border:1px solid #ccc;
				width:627px;
				height:497px;
				float:left;
				margin:5px;
				overflow-y: scroll;
			}
			.photo_area .pgroup{
				border-bottom:1px solid #ccc; 
				background: #eee;
				width:610px;
				height:170px;
				float:left;
				position: relative;
			}
			.photo_area .pgroup .p1 {
				position: relative;
				top:45px;
				left:20px;
				display:block;
				background: orange ;
				width:80px;
				height:60px;
			}
			.photo_area .pgroup .p1 img {
				width:80px;
				height:60px;
			}
			.photo_area .pgroup .p1 .px1{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_area .pgroup .p2 {
				position:absolute;
				top:25px;
				left:185px;
				display:block;
				background: red ;
				width:150px;
				height:100px;
			}
			.photo_area .pgroup .p2 img {
				width:150px;
				height:100px;
			}
			.photo_area .pgroup .p2 .px2{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_area .pgroup .p3 {
				position:absolute;
				top:15px;
				left:420px;
				display:block;
				background: #bbb ;
				width:170px;
				height:120px;
			}
			.photo_area .pgroup .p3 img {
				width:170px;
				height:120px;
			}
			.photo_area .pgroup .p3 .px3{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_click{
				background: #eee ;
				border:1px solid #ccc;
				width:114px;
				height:497px;
				float:left;
				margin:5px;
				margin-left:0px;
				text-align: center;
			}
		</style>
		<div data-options="title: '上传图片',refreshable: false, iconCls: 'icon-standard-layout-header'">
			<div class="photo_area">
				<input type="hidden" value="" name="photos_path">
				<!-- 
				<div class="pgroup">
					<div class="p1"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px1">1400×880</p></div>
					<div class="p2"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px2">1400×880</p></div>
					<div class="p3"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px3">1400×880</p></div>
				</div>
				 -->
			</div>
			<div  id="container" class="photo_click">
				<a id="pickfiles" style="margin:8px;" class="easyui-linkbutton" data-options="plain: false, iconCls: 'icon-hamburg-folder'" >选择文件</a>
			</div>
		</div>
	</div>
</form>


