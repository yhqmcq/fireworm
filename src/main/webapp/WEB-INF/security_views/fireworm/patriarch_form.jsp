<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<script type="text/javascript">
	var form_url = $tools.basePath+"/fireworm/patriarchAction/add.do" ;
	
	$(function() {
		//编辑，加载表单数据
		if($('input[name=id]').val().length > 0) {
			form_url = $tools.basePath+"/fireworm/patriarchAction/edit.do" ;
			$.post($tools.basePath+"/fireworm/patriarchAction/get.do", {id:$('input[name=id]').val()}, function(result) {
				if (result.id != undefined) {
					$('form').form('load', {
						'name' : result.name,
						'sex' : result.sex,
						'mobile' : result.mobile,
						'password' : result.password,
						'email' : result.email,
						'addr' : result.addr,
						'remark' : result.remark
					});
					
				}
			}, 'json');
		}
		
	});
	
	//提交表单数据
	var submitNow = function($d, $dg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				$dg.datagrid('reload');
				$.easyui.loaded();
				alertify.success(result.msg); 
				$d.dialog("close");
			} else {
				$.easyui.loaded();
				alertify.error(result.msg);
			}
		}, 'json').error(function(){$.easyui.loaded();});
	};
	
	//验证表单
	var submitForm = function($d, $dg) {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $dg) ; ;
		}
	};
	
	
</script>
<div class="dialog_centent">
	<form id="form" class="easyui-form">
		<input type="hidden" name="id" value="${id}" />
		<table class="grid">
			<tr>
				<td>名称：</td>
				<td><input name="name" class="easyui-validatebox" type="text" data-options="required: true" /></td>
				<td>性别：</td>
				<td>
					<select class="easyui-combobox"  style="width:218px;height:30px;" name="sex" data-options="
						valueField: 'label', textField: 'value', editable: false, value : '男',
						data: [{ label: '男', value: '男' },{ label: '女', value: '女' }],
						panelHeight:'auto', editable:false"></select>
				</td>
			</tr>
			<tr>
				<td>手机号码：</td>
				<td><input name="mobile" class="easyui-validatebox" type="text" data-options="required: true, prompt: '多个号码，请用逗号”，“隔开'" /></td>
				<td>登陆密码：</td>
				<td><input name="password" class="easyui-validatebox" type="text" /></td>
			</tr>
			<tr>
				<td>邮箱地址：</td>
				<td><input name="email" class="easyui-validatebox" type="text" /></td>
				<td>联系地址：</td>
				<td><input name="addr" class="easyui-validatebox" type="text" /></td>
			</tr>
			<tr>
				<td>备注：</td>
				<td colspan="3"><textarea name="remark" style="width:552px;max-width:552px;min-width:552px;height:100px;max-height:100px;min-height: 100px" class="easyui-validatebox"></textarea></td>
			</tr>
		</table>
	</form>

</div>
