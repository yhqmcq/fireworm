<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>儿童信息管理</title>
<%@ include file="/common/header/meta.jsp"%>
<%@ include file="/common/header/script.jsp"%>

<script type="text/javascript">
	var $dg ;
	$(function() {
		$dg = $("#d1").datagrid({
			url: $tools.basePath+"/fireworm/childrenAction/datagrid.do",
			title: '儿童信息管理', method: "post", idField: 'id', fit: true, border: false,
			remoteSort: false, toolbar: '#buttonbar', striped:true, pagination: true, singleSelect: true,
			frozenColumns: [[
			    { field: 'ck', checkbox: true },
			    { field: 'id', title: 'ID', width: 80, sortable: true, hidden: true },
			    { field: 'name', title: '姓名', width: 100, sortable: true }
			]],
			columns: [[
			    { field: 'sname', title: '小名', width:100, sortable: true},
			    { field: 'sex', title: '性别', width:40, align: 'center', sortable: true},
			    { field: 'brith', title: '出生日期', width: 80, sortable: true, formatter: function(value, row){
			    	if(undefined != value) { return $.date.format(value, "yyyy-MM-dd") ; }
			    } },
			    { field: 'lost_time', title: '丢失时间', width: 140, sortable: true },
			    { field: 'lost_height', title: '丢前身高', width: 60, sortable: true, formatter: function(value, row){
			    	if(undefined != value) { return value + " CM" ; }
			    } },
			    { field: 'isPay', title: '是否酬谢', width: 60, align: 'center', sortable: true },
			    { field: 'money', title: '酬谢金额', width: 60, sortable: true },
			    { field: 'address', title: '丢失位置', width: 250, sortable: true, formatter: function(value, row){
			    	var addr = "" ;
			    	if(row.province != undefined) {
			    		addr += row.province+" " ;
			    	}if(row.city != undefined) {
			    		addr += row.city+" " ;
			    	}if(row.county != undefined+" ") {
			    		addr += row.county ;
			    	}if(row.lost_addr != undefined+" ") {
			    		addr += row.lost_addr+" " ;
			    	}
			    	return addr ;
			    } },
			    { field: 'created', title: '新建日期', width: 140, sortable: true }
			]],
			enableHeaderClickMenu: true, enableHeaderContextMenu: true, selectOnRowContextMenu: false, pagingMenu: { submenu: false }
	    });
	});
	
	function form_edit(form) {
		var form_url = $tools.basePath+"/fireworm/childrenAction/children_form.do" ;
		if("E" == form) {
			var node = $dg.datagrid('getSelected');
			if (node) {
				form_url = $tools.basePath+"/fireworm/childrenAction/children_form.do?id="+node.id ;
			} else {
				$.easyui.messager.show({ icon: "info", msg: "请选择一条记录！" });
				return ;
			}
		} 
		var $d = $.easyui.showDialog({
			href: form_url, title: "表单", iniframe: false, topMost: true,
			width: 780,
			height: 618,
            enableApplyButton: false, enableCloseButton: false,  enableSaveButton: false,
            buttons : [ 
              { text : '确定', iconCls : 'ext_save', handler : function() { $.easyui.parent.submitForm($d, $dg) ; } },
              { text : '关闭', iconCls : 'ext_cancel', handler : function() { $d.dialog('destroy'); } } 
           	]
        });
	}
	
	function del() {
		var rows = $dg.datagrid('getChecked');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm("您确定要进行该操作？", function (c) { 
				if(c) {
					$.easyui.loading({ msg: "数据删除中，请稍等..."});
					for ( var i = 0; i < rows.length; i++) {
						ids.push(rows[i].id);
					}
					$.post($tools.basePath+"/fireworm/childrenAction/delete.do", {ids : ids.join(',')}, function(result) {
						if (result.status) {
							$dg.datagrid('clearSelections');$dg.datagrid('clearChecked');$dg.datagrid('reload') ;
							$.easyui.messager.show({ icon: "info", msg: result.msg });
							$.easyui.loaded();
						} else {
							$.easyui.messager.show({ icon: "info", msg: result.msg });
							$.easyui.loaded();
						}
					}, 'json').error(function() { $.easyui.loaded(); });
				}
			});
		} else {
			$.easyui.messager.show({ icon: "info", msg: "请选择一条记录！" });
		}
	}
	
</script>

</head>

<body style="padding: 0px; margin: 0px;">
	<div class="easyui-layout" data-options="fit: true">
		<div data-options="region: 'center', border: false" style="overflow: hidden;">
			<div id="d1">
				<div id="buttonbar">
                    <a id="btn1" onClick="form_edit('A');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_add'">添加</a>
                    <a id="btn2" onClick="form_edit('E');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_edit'">编辑</a>
                    <a id="btn3" onClick="del();" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_remove'">删除</a>
                    <a id="btn4" onclick="$dg.datagrid('reload');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_reload'">刷新</a>
                </div>
			</div>
		</div>
	</div>	
</body>
</html>