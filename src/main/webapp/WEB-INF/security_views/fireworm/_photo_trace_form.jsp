<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<script type="text/javascript">
	
	$(function() {
		var photo_area = $(".photo_area") ;
		uploader = new plupload.Uploader({
			runtimes : 'html5,flash,html4',
			flash_swf_url : $tools.basePath+'/js/plugins/plupload-2.1.1/js/Moxie.swf',	//Flash支持
			url : $tools.basePath+'/upload?upload_path=/lost/system&pc=true&folderType=1&fileNameType=1',
			browse_button : 'pickfiles', 
			//chunk_size : '5mb',//把大文件分割  
			container: 'container', 
			init: {
				PostInit: function() {
				},
				FilesAdded: function(up, files) {
					uploader.start();
					$.easyui.loading({ msg: "图片上传中，请稍等..." });
				},
				FileUploaded: function(up, file, info) {
					var response = $.parseJSON(info.response);
					if (response.status) {
						
						$.post($tools.basePath+"/fireworm/photoTraceAction/doNotNeedAuth_resize_photo.do", {
							web_root: response.fileinfo.web_root,
							photos_path: response.fileinfo.saveDir+response.fileinfo.newName
						}, function(result) {
							if(result.status) {
								var photo_group = "" +
												  "<div class='pgroup'>"+
								 				  "<div class='p1'><img src='"+result.obj[1]+"'><p class='px1'>80×60</p></div>"+
												  "<div class='p2'><img src='"+result.obj[2]+"'><p class='px2'>150×100</p></div>"+
												  "<div class='p3'><img src='"+result.obj[3]+"'><p class='px3'>170×120</p></div>"+
											      "</div>";
								photo_area.append(photo_group) ;
								$.easyui.loaded();
								
								var photos_path = $("input[name=photos_path]") ;
								if(photos_path.val() != "") {
									photos_path.val(photos_path.val()+"|"+result.obj[0]) ;
								} else {
									photos_path.val(result.obj[0]);
								}
							} else {
								$.easyui.loaded();alertify.error(result.msg);
							}
						}, 'json').error(function() {
							$.easyui.loaded();alertify.error(result.msg);
						});
					} else {
						$.easyui.loaded();alertify.error("文件上传失败："+response.msg);
					}
				},
				UploadProgress: function(up, file) {
				},
				Error: function(up, err) {
					$.easyui.loaded();alertify.error("错误："+ err.code+"---"+err.message+"---"(err.file ? ", File: " + err.file.name : ""));
					up.refresh(); 
				}
			}
		});
		uploader.init();
		
	});
	
	//提交表单数据
	var submitNow = function($d, $dg) {
		$.post(form_url, $("#form").form("getData"), function(result) {
			if (result.status) {
				alertify.success(result.msg);$.easyui.loaded(); $d.dialog("close") ;
			} else {
				$.easyui.loaded();alertify.error(result.msg);
			}
		}, 'json').error(function(){$.easyui.loaded();});
	};
	
	//验证表单
	var submitForm = function($d, $dg) {
		if($('#form').form('validate')) {
			$.easyui.loading({ msg: "数据提交中，请稍等..." });
			submitNow($d, $dg) ; ;
		}
	};
	
	
</script>
<style>
			.photo_area{ 
				border:1px solid #ccc;
				width:627px;
				height:497px;
				float:left;
				margin:5px;
				overflow-y: scroll;
			}
			.photo_area .pgroup{
				border-bottom:1px solid #ccc; 
				background: #eee;
				width:610px;
				height:170px;
				float:left;
				position: relative;
			}
			.photo_area .pgroup .p1 {
				position: relative;
				top:45px;
				left:20px;
				display:block;
				background: orange ;
				width:80px;
				height:60px;
			}
			.photo_area .pgroup .p1 img {
				width:80px;
				height:60px;
			}
			.photo_area .pgroup .p1 .px1{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_area .pgroup .p2 {
				position:absolute;
				top:25px;
				left:185px;
				display:block;
				background: red ;
				width:150px;
				height:100px;
			}
			.photo_area .pgroup .p2 img {
				width:150px;
				height:100px;
			}
			.photo_area .pgroup .p2 .px2{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_area .pgroup .p3 {
				position:absolute;
				top:15px;
				left:420px;
				display:block;
				background: #bbb ;
				width:170px;
				height:120px;
			}
			.photo_area .pgroup .p3 img {
				width:170px;
				height:120px;
			}
			.photo_area .pgroup .p3 .px3{
				position:absolute;
				left:5px;bottom:-20px;
			}
			.photo_click{
				background: #eee ;
				border:1px solid #ccc;
				width:114px;
				height:497px;
				float:left;
				margin:5px;
				margin-left:0px;
				text-align: center;
			}
		</style>
<div class="dialog_centent">
	<form id="form" class="easyui-form">
		<div class="photo_area">
			<input type="hidden" value="" name="photos_path">
			<!-- 
			<div class="pgroup">
				<div class="p1"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px1">1400×880</p></div>
				<div class="p2"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px2">1400×880</p></div>
				<div class="p3"><img src="${pageContext.request.contextPath}/images/pass_.png"><p class="px3">1400×880</p></div>
			</div>
			 -->
		</div>
		<div  id="container" class="photo_click">
			<a id="pickfiles" style="margin:8px;" class="easyui-linkbutton" data-options="plain: false, iconCls: 'icon-hamburg-folder'" >选择文件</a>
		</div>
	</form>

</div>
