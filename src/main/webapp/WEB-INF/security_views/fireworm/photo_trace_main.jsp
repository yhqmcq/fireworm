<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>人事信息</title>
<%@ include file="/common/header/meta.jsp"%>
<%@ include file="/common/header/script.jsp"%>

<script type="text/javascript">
	$tools.loadPlugin("pubu");
	$tools.loadPlugin("fancyBox");
	$(function() {
		$('#pg').pagination();
		$('.fancybox-buttons').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
			closeBtn  : false,
			minWidth: 650,
			minHeight: 450,
			helpers : {
				title : {type : 'inside'},
				buttons	: {}
			},
			afterLoad : function() {
				this.title = '图片 ' + (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
			}
		});
		
		var province = $("#province").combobox() ;
		var city = $("#city").combobox();
		city.combobox("setValue","===请选择===");
		province.combobox({
			url: $tools.basePath+"/sysmgr/areaAction/doNotNeedSession_getAreas.do",
			valueField: "id",
			textField: "areaName",
			value: "===请选择===",
			onSelect: function(node) {
	            var province = $('#province').combobox('getValue');     
	            if(province!=''){
	            	city.combobox({
	            		url: $tools.basePath+"/sysmgr/areaAction/doNotNeedSession_getAreas.do?id="+province,
	            		valueField: "id",
	        			textField: "areaName",
	        			value: "===请选择===",
	        			onSelect: function(node) {
       				 		$('#city').combobox('setValue', node.areaName);
	        			}
	            	});
	            }
	            $('#province').combobox('setValue', node.areaName);
			}
		});
		
		var searchOpts = $("#topSearchbox").searchbox("options"), searcher = searchOpts.searcher;
        searchOpts.searcher = function (value, name) {
            if ($.isFunction(searcher)) { searcher.apply(this, arguments); }
            alert() ;
        };
        
        $.post($tools.basePath+"/fireworm/photoTraceAction/datagrid.do", null, function(result) {
			if (result.rows.length > 0) {
				var bp = $("#pubu");
				$.each(result.rows, function(i, p) {
					var ps = p.photos_path.split(",") ;
					var _div = "<div class='box'>"+
							   "    <div class='pic'>"+
							   "    <a class='fancybox-buttons' data-fancybox-group='button' href='<%=webroot%>"+ps[0]+"'>"+
						       "        <img src='<%=webroot%>"+ps[3]+"' />"+
						       "    </a>"+
						       "    </div>"+
						       "</div>" ;
					bp.append(_div) ;
				}) ;
				
				$.easyui.loaded();
			} else {
				$.easyui.loaded();
				alertify.warning("数据加载失败！");
			}
		}, 'json').error(function() { $.easyui.loaded(); });
	});
	
	function upload_photo() {
		var $d = $.easyui.showDialog({
			href: $tools.basePath+"/fireworm/photoTraceAction/photo_trace_form.do",
			title: "表单", iniframe: false, topMost: true, width: 850, height: 400,
            enableApplyButton: false, enableCloseButton: false,  enableSaveButton: false
        });
	}
	
</script>

</head>

<body style="padding: 0px; margin: 0px;">
	<div class="easyui-layout" data-options="fit: true">
		<div data-options="region: 'north', border: false" style="overflow: hidden;display: table;height:70px;">
			<div id="tt" class="easyui-toolbar">
		        <a onclick="upload_photo();" class="easyui-linkbutton" data-options="plain: true, iconCls: 'icon-hamburg-zoom'">上传</a>
		        <div class="dialog-tool-separator"></div>
		        <a class="easyui-linkbutton" data-options="plain: true, iconCls: 'icon-hamburg-config'">配置</a>
		        <span>-</span>
		        <a class="easyui-linkbutton" data-options="plain: true, iconCls: 'icon-hamburg-pencil'">编辑</a>
		        <a class="easyui-linkbutton" data-options="plain: true, iconCls: 'icon-hamburg-busy'">删除</a>
		    </div> 
		    <div class="easyui-toolbar">
		    	<input style="width:120px;height:25px;" id="province" name="province"></input>
				<input style="width:120px;height:25px;" id="city" name="city"></input>
		        <span>-</span>
		        <input name="brith" style="height:25px;width:100px;" class="easyui-my97" type="text" readonly="readonly" />
				<span>至</span>
                <input name="brith" style="height:25px;width:100px;" class="easyui-my97" type="text" readonly="readonly" data-options="maxDate:'new Date()'" />
                <span>-</span>
		        <input id="topSearchbox" class="easyui-searchbox" data-options="width: 283, height: 26, prompt: '输入要查找的内容关键词', menu: '#topSearchboxMenu'" />
				<div id="topSearchboxMenu" style="width: 85px;">
                    <div data-options="name:'id', iconCls: 'icon-hamburg-zoom'">手机号码</div>
                </div>
		    </div> 
		</div>
		
		<div data-options="region: 'center', border: false" style="overflow: hidden;padding:3px;">
			<div class="dialog_centent" style="background-color: #F5F5F5;border-radius: 4px;border: 1px solid #E3E3E3;padding:5px;">
				
				<div id="pg"></div>
				
				<div style="overflow-y:auto;text-align:center;width:100%;height:500px;background: #FFF;box-shadow: 0 1px 3px #ccc;border: 1px solid #ccc;">
					<div id="pubu"></div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>