<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>家长信息管理</title>
<%@ include file="/common/header/meta.jsp"%>
<%@ include file="/common/header/script.jsp"%>

<script type="text/javascript">
	var $dg ;
	$(function() {
		$dg = $("#d1").datagrid({
			url: $tools.basePath+"/fireworm/patriarchAction/datagrid.do",
			title: '家长信息管理', method: "post", idField: 'id', fit: true, border: false,
			remoteSort: false, toolbar: '#buttonbar', striped:true, pagination: true, singleSelect: true,
			frozenColumns: [[
			    { field: 'ck', checkbox: true },
			    { field: 'id', title: 'ID', hidden: true, width: 80, sortable: true },
			    { field: 'name', title: '姓名', width: 100, sortable: true }
			]],
			columns: [[
			    { field: 'sex', title: '性别', width: 50, sortable: true },
			    { field: 'mobile', title: '手机号码', width: 100, sortable: true, formatter: function(value, row){
			    	var txt = "" ;
			    	var sp = value.split(",") ;
			    	$.each(sp, function(i){
			    		txt += sp[i]+"<br />" ;
			    	}) ;
			    	return txt ;
			    } },
			    { field: 'email', title: '邮箱地址', width: 180, sortable: true },
			    { field: 'addr', title: '联系地址', width: 300, sortable: true },
			    { field: 'updateDateTime', title: '更新时间', width: 140, sortable: true}, 
			    { field: 'created', title: '新建日期', width: 140, sortable: true },
			    { field: 'remark', title: '备注', width: 300, sortable: true}
			]],
			enableHeaderClickMenu: true, enableHeaderContextMenu: true, selectOnRowContextMenu: false, pagingMenu: { submenu: false }
	    });
	});
	
	function form_edit(form) {
		var form_url = $tools.basePath+"/fireworm/patriarchAction/patriarch_form.do" ;
		if("E" == form) {
			var node = $dg.datagrid('getSelected');
			if (node) {
				form_url = $tools.basePath+"/fireworm/patriarchAction/patriarch_form.do?id="+node.id ;
			} else {
				alertify.warning("请选择一条记录！");
				return ;
			}
		}
		var $d = $.easyui.showDialog({
			href: form_url, title: "表单", iniframe: false, topMost: true,
			width: (700 > parent.$tools.getInner().width? parent.$tools.getInner().width-100:700),
			height: (400 > parent.$tools.getInner().height? parent.$tools.getInner().height-60:400),
            enableApplyButton: false, enableCloseButton: false,  enableSaveButton: false,
            buttons : [ 
              { text : '确定', iconCls : 'ext_save', handler : function() { $.easyui.parent.submitForm($d, $dg) ; } },
              { text : '关闭', iconCls : 'ext_cancel', handler : function() { $d.dialog('destroy'); } } 
           	]
        });
	}
	
	function del() {
		var rows = $dg.datagrid('getChecked');
		var ids = [];
		if (rows.length > 0) {
			$.messager.confirm("您确定要进行该操作？", function (c) { 
				if(c) {
					$.easyui.loading({ msg: "数据删除中，请稍等..."});
					for ( var i = 0; i < rows.length; i++) {
						ids.push(rows[i].id);
					}
					$.post($tools.basePath+"/fireworm/patriarchAction/delete.do", {ids : ids.join(',')}, function(result) {
						if (result.status) {
							$dg.datagrid('clearSelections');$dg.datagrid('clearChecked');$dg.datagrid('reload') ;
							$.easyui.loaded();
							alertify.success(result.msg);
						} else {
							$.easyui.loaded();
							alertify.warning(result.msg);
						}
					}, 'json').error(function() { $.easyui.loaded(); });
				}
			});
		} else {
			alertify.warning("请选择一条记录！");
		}
	}
	
</script>

</head>

<body style="padding: 0px; margin: 0px;">
	<div class="easyui-layout" data-options="fit: true">
		<div data-options="region: 'center', border: false" style="overflow: hidden;">
			<div id="d1">
				<div id="buttonbar">
                    <a id="btn1" onClick="form_edit('A');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_add'">添加</a>
                    <a id="btn2" onClick="form_edit('E');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_edit'">编辑</a>
                    <a id="btn3" onClick="del();" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_remove'">删除</a>
                    <a id="btn4" onclick="$dg.datagrid('reload');" class="easyui-linkbutton" data-options="plain: true, iconCls: 'ext_reload'">刷新</a>
                </div>
			</div>
		</div>
	</div>	
</body>
</html>