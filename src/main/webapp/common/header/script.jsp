<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%String webroot = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();%>
<%String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<%
java.util.Map<String, Cookie> cookieMap = new java.util.HashMap<String, Cookie>();
Cookie[] cookies = request.getCookies();
	if (null != cookies) {
		for (Cookie cookie : cookies) {
		cookieMap.put(cookie.getName(), cookie);
	}
}
String themeName = "bootstrap";
if (cookieMap.containsKey("themeName")) {
	Cookie cookie = (Cookie) cookieMap.get("themeName");
	themeName = cookie.getValue();
}
%>

<link href="<%=basePath%>/js/jquery-easyui-theme/<%=themeName%>/easyui.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/js/jquery-easyui-theme/icon.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/js/icons/icon-all.css" rel="stylesheet" type="text/css" />

<script src="<%=basePath%>/js/jquery/jquery-1.11.0.min.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/tools/base.tools.js" type="text/javascript"></script>
<script type="text/javascript">$tools.basePath = '<%=basePath%>';</script>

<script src="<%=basePath%>/js/jquery-easyui-1.3.6/jquery.easyui.min.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/tools/jquery.jdirk.js" type="text/javascript"></script>

<link href="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.css" rel="stylesheet" type="text/css" />

<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.linkbutton.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.form.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.validatebox.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.combo.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.combobox.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.menu.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.searchbox.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.panel.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.window.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.dialog.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.layout.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.tree.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.datagrid.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.treegrid.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.combogrid.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.combotree.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.tabs.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.theme.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/icons/jeasyui.icons.all.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.icons.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-toolbar/jquery.toolbar.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-comboicons/jquery.comboicons.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/jeasyui-extensions/jeasyui.extensions.gridselector.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-comboselector/jquery.comboselector.js" type="text/javascript"></script>


<script src="<%=basePath%>/resources/admin/plugin-import.js" type="text/javascript"></script>

<link href="<%=basePath%>/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/themes/default/css/ueditor.css" rel="stylesheet"/>
<script src="<%=basePath%>/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/ueditor.config.js"></script>
<script src="<%=basePath%>/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/ueditor.all.js"></script>
<script src="<%=basePath%>/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/lang/zh-cn/zh-cn.js"></script>
<script src="<%=basePath%>/js/plugins/My97DatePicker/WdatePicker.js"></script>

  
<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-my97/jquery.my97.js" type="text/javascript"></script>
<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-ueditor/jquery.ueditor.js"></script>

<script src="<%=basePath%>/js/jeasyui-extensions/jquery-easyui-portal/jquery.portal.js" type="text/javascript"></script>

<script src="<%=basePath%>/js/jeasyui-other-extension/easyui-cascade.js" type="text/javascript"></script>

<script type="text/javascript" src="<%=basePath%>/js/plugins/plupload-2.1.1/js/plupload.full.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=basePath%>/js/plugins/alertify/themes/alertify.core.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath%>/js/plugins/alertify/themes/alertify.default.css" />
<script type="text/javascript" charset="utf-8" src="<%=basePath%>/js/plugins/alertify/lib/alertify.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=basePath%>/js/tools/alertify_ext.js"></script>


<script src="<%=basePath%>/js/tools/jquery-jrumble.js" type="text/javascript"></script>

<!--导入首页启动时需要的相应资源文件(首页相应功能的 js 库、css样式以及渲染首页界面的 js 文件)-->
<link href="<%=basePath%>/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/resources/admin/admin-index.css" rel="stylesheet" type="text/css" />

