﻿(function () {
    var plugins = {
        ueditor: [
            "<link href=\""+$tools.basePath+"/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/themes/default/css/ueditor.css\" rel=\"stylesheet\"/>",
            "<script src=\""+$tools.basePath+"/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/ueditor.config.js\"></script>",
            "<script src=\""+$tools.basePath+"/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/ueditor.all.js\"></script>",
            "<script src=\""+$tools.basePath+"/js/plugins/ueditor/ueditor1_3_6-utf8-jsp/lang/zh-cn/zh-cn.js\"></script>"
        ],
        fancyBox: [
            "<link href=\""+$tools.basePath+"/js/plugins/fancyBox/jquery.fancybox.css\" rel=\"stylesheet\"/>",
            "<script src=\""+$tools.basePath+"/js/plugins/fancyBox/jquery.fancybox.pack.js\"></script>",
            "<link href=\""+$tools.basePath+"/js/plugins/fancyBox/helpers/jquery.fancybox-buttons.css\" rel=\"stylesheet\"/>",
            "<script src=\""+$tools.basePath+"/js/plugins/fancyBox/helpers/jquery.fancybox-buttons.js\"></script>",
            "<link href=\""+$tools.basePath+"/js/plugins/fancyBox/helpers/jquery.fancybox-thumbs.css\" rel=\"stylesheet\"/>",
            "<script src=\""+$tools.basePath+"/js/plugins/fancyBox/helpers/jquery.fancybox-thumbs.js\"></script>",
            "<script src=\""+$tools.basePath+"/js/plugins/fancyBox/helpers/jquery.fancybox-media.js\"></script>"
        ],
        pubu: [
          "<link href=\""+$tools.basePath+"/js/plugins/pubu/css/pubu.css\" rel=\"stylesheet\"/>",
          "<script src=\""+$tools.basePath+"/js/plugins/pubu/js/pubu.js\"></script>",
      ]
    };
    //加载插件相关依赖
    $tools.loadPlugin = function(name) {
    	if (name) {
            var names = String(name).split(",");
            for (var i = 0; i < names.length; i++) {
                var plugin = plugins[names[i]];
                if (plugin) {
                    $.each(plugin, function (index, n) {
                        document.write(n);
                    });
                }
            }
        }
    };
})();