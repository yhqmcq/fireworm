package com.galaxy.system.web.form;

import com.galaxy.common.web.page.PageHelper;

public class PermitForm extends PageHelper {
	
	private String id ;
	
	private String permitCode ;
	
	/** 权限值permitValue = moduleValue_actionValue */
	private String permitValue ;
	
	private String action_id ;
	
	private String module_id ;
	
	private String pid ;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPermitCode() {
		return permitCode;
	}

	public void setPermitCode(String permitCode) {
		this.permitCode = permitCode;
	}

	public String getPermitValue() {
		return permitValue;
	}

	public void setPermitValue(String permitValue) {
		this.permitValue = permitValue;
	}

	public String getAction_id() {
		return action_id;
	}

	public void setAction_id(String action_id) {
		this.action_id = action_id;
	}

	public String getModule_id() {
		return module_id;
	}

	public void setModule_id(String module_id) {
		this.module_id = module_id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
	

}
