package com.galaxy.system.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.Json;
import com.galaxy.common.web.springmvc.RealPathResolver;
import com.galaxy.system.service.FileManagerServiceI;
import com.galaxy.system.web.form.FileWrapForm;

@Controller
@RequestMapping("/sysmgr/filemanager")
public class FileManager extends BaseController {

	@Autowired
	private FileManagerServiceI fileservice;
	
	@Autowired
	private RealPathResolver realPathResolver ;
	
	@RequestMapping("/file_main.do")
	public String file_main(HttpServletRequest request) throws Exception {
		request.setAttribute("root", "/"+Constants.FILE_ROOT) ;
		return  Constants.SYSMGR + "file_main" ;
	}
	
	@RequestMapping("/upload.do")
	public String upload(FileWrapForm form, HttpServletRequest request) throws Exception {
		request.setAttribute("upload_path", form.getPath()) ;
		return  Constants.SYSMGR + "file_upload" ;
	}
	
	/**
	 * 获取文件管理根目录树
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/file_treelist.do")
	@ResponseBody
	public List<FileWrapForm> file_treelist(FileWrapForm form) throws Exception {
		try {
			return this.fileservice.treeListFile(form) ;
		} catch (Exception e) {
			e.printStackTrace();
			throw e ;
		}
	}
	
	/**
	 * 创建目录
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/file_createDir.do")
	@ResponseBody
	public Json file_createDir(FileWrapForm form) throws Exception {
		Json json = new Json() ;
		try {
			this.fileservice.createDir(form) ;
			json.setStatus(true) ;
		} catch (Exception e) {
			json.setMsg(e.getMessage()) ;
		}
		return json ;
	}
	
	/**
	 * 删除目录或文件
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/file_deleteff.do")
	@ResponseBody
	public Json file_delff(FileWrapForm form) throws Exception {
		Json json = new Json() ;
		try {
			int deleteFF = this.fileservice.deleteFF(form) ;
			json.setMsg("总共删除文件或目录["+deleteFF+"]个") ;
			json.setStatus(true) ;
		} catch (Exception e) {
			json.setMsg(e.getMessage()) ;
		}
		return json ;
	}
	
	/**
	 * 复制文件或目录
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/file_copyff.do")
	@ResponseBody
	public Json file_copy(FileWrapForm form) throws Exception {
		Json json = new Json() ;
		try {
			this.fileservice.copyFF(form) ;
			json.setStatus(true) ;
		} catch (Exception e) {
			json.setMsg(e.getMessage()) ;
		}
		return json ;
	}
	
	/**
	 * 移动文件或目录
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/file_moveff.do")
	@ResponseBody
	public Json file_move(FileWrapForm form) throws Exception {
		Json json = new Json() ;
		try {
			this.fileservice.moveFF(form) ;
			json.setStatus(true) ;
		} catch (Exception e) {
			json.setMsg(e.getMessage()) ;
		}
		return json ;
	}
	
	
}
