package com.galaxy.system.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.service.RoleServiceI;
import com.galaxy.system.web.form.RoleForm;

@Controller
@RequestMapping("/sysmgr/roleAction")
public class RoleAction extends BaseController{
	
	@Autowired
	private RoleServiceI roleService ;
	
	@RequestMapping("/role_main.do")
	public String role_main() {
		return Constants.SYSMGR + "role_main" ;
	}
	
	@RequestMapping("/role_form.do")
	public String role_form(RoleForm form, HttpServletRequest request) {
		if(null != form.getId() && !"".equals(form.getId())) {
			request.setAttribute("id", form.getId()) ;
		}
		return Constants.SYSMGR + "role_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public RoleForm get(RoleForm form, HttpServletRequest request) {
		return this.roleService.get(form) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	public Json add(RoleForm form) {
		return this.roleService.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(RoleForm form) {
		return this.roleService.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(RoleForm form) {
		return this.roleService.delete(form) ;
	}
	
	@RequestMapping("/treegrid.do")
	@ResponseBody
	public List<RoleForm> treegrid(RoleForm form) {
		return this.roleService.treegrid(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(RoleForm form) {
		return this.roleService.datagrid(form) ;
	}
	
	@RequestMapping("/getPermission.do")
	@ResponseBody
	public RoleForm getPermission(RoleForm form) {
		return this.roleService.getPermission(form) ;
	}
	
	@RequestMapping("/set_grant.do")
	@ResponseBody
	public Json set_grant(RoleForm form) {
		return this.roleService.set_grant(form) ;
	}
	
}
