package com.galaxy.system.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.service.TaskSchedulerServiceI;
import com.galaxy.system.web.form.TaskForm;

@Controller
@RequestMapping("/sysmgr/task")
public class TaskScheduler extends BaseController {
	
	@Autowired
	private TaskSchedulerServiceI taskservice ;
	
	@RequestMapping("/task_main.do")
	public String task_main() {
		return  Constants.SYSMGR + "task_main" ;
	}
	
	@RequestMapping("/doNotNeedAuth_task_cron.do")
	public String task_cron(TaskForm form, HttpServletRequest request) {
		return Constants.SYSMGR + "task_cron" ;
	}
	
	@RequestMapping("/task_form.do")
	public String task_form(TaskForm form, HttpServletRequest request) {
		if(null != form.getId() && !"".equals(form.getId())) {
			request.setAttribute("id", form.getId()) ;
		}
		return Constants.SYSMGR + "task_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public TaskForm get(TaskForm form, HttpServletRequest request) {
		return this.taskservice.get(form.getId()) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	public Json add(TaskForm form) {
		return this.taskservice.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(TaskForm form) {
		return this.taskservice.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(TaskForm form) {
		return this.taskservice.delete(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(TaskForm form) {
		return this.taskservice.datagrid(form) ;
	}
}
