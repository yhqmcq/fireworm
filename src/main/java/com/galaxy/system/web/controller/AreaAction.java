package com.galaxy.system.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.web.BaseController;
import com.galaxy.system.service.AreaServiceI;
import com.galaxy.system.web.form.AreaForm;

@Controller
@RequestMapping("/sysmgr/areaAction")
public class AreaAction extends BaseController {

	@Autowired
	private AreaServiceI areaService ;
	
	@RequestMapping("/doNotNeedSession_getAreas.do")
	@ResponseBody
	public List<AreaForm> doNotNeedSession_getAreas(AreaForm form) throws Exception {
		return this.areaService.findAreas(form) ;
	}
	
	@RequestMapping("/doNotNeedSession_getProvince.do")
	@ResponseBody
	public List<AreaForm> doNotNeedSession_getProvince(AreaForm form) throws Exception {
		return this.areaService.findProvince(form) ;
	}
	
	@RequestMapping("/doNotNeedSession_getCity.do")
	@ResponseBody
	public List<AreaForm> doNotNeedSession_getCity(AreaForm form) throws Exception {
		return this.areaService.findCity(form) ;
	}
	
	@RequestMapping("/getCounty.do")
	@ResponseBody
	public List<AreaForm> getCounty(AreaForm form) throws Exception {
		return this.areaService.findCounty(form) ;
	}
	
	
}
