package com.galaxy.system.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.util.web.IpUtil;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.common.web.page.LoginInfoSession;
import com.galaxy.system.service.UserServiceI;
import com.galaxy.system.web.form.UserForm;

@Controller
@RequestMapping("/sysmgr/userAction")
public class UserAction extends BaseController {

	@Autowired
	private UserServiceI empService ;
	
	@RequestMapping("/user_main.do")
	public String user_main(){
		return  Constants.SYSMGR + "user_main" ;
	}
	
	@RequestMapping("/user_form.do")
	public String user_form(UserForm form, HttpServletRequest request){
		if(null != form.getId() && !"".equals(form.getId())) {
			request.setAttribute("id", form.getId()) ;
		}
		return Constants.SYSMGR + "user_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public UserForm get(UserForm form, HttpServletRequest request){
		return this.empService.get(form.getId()) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	synchronized public Json add(UserForm form){
		return this.empService.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(UserForm form){
		return this.empService.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(UserForm form){
		return this.empService.delete(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(UserForm form){
		return this.empService.datagrid(form) ;
	}
	
	@RequestMapping("/user_permit_main.do")
	public String user_permit_main(){
		return  Constants.SYSMGR + "user_permit" ;
	}
	
	@RequestMapping("/user_module_permit.do")
	public String user_module_permit(){
		return  Constants.SYSMGR + "user_module_permit" ;
	}
	
	@RequestMapping("/set_permit.do")
	@ResponseBody
	public Json set_permit(UserForm form) throws Exception {
		return this.empService.set_permit(form) ;
	}
	
	@RequestMapping("/getPermission.do")
	@ResponseBody
	public UserForm getPermission(UserForm form) throws Exception {
		return this.empService.getPermission(form) ;
	}
	
	@RequestMapping("/get_module_permit.do")
	@ResponseBody
	public UserForm get_module_permit(UserForm form) throws Exception {
		return this.empService.get_module_permit(form) ;
	}
	
	@RequestMapping("/set_module_permit.do")
	@ResponseBody
	public Json set_module_permit(UserForm form) {
		return this.empService.set_module_permit(form) ;
	}
	
	
	/**
	 * 登陆验证
	 * @throws Exception 
	 */
	@RequestMapping("/doNotNeedSession_login.do")
	@ResponseBody
	public Json doNotNeedSession_login(UserForm form, HttpServletRequest request) throws Exception {
		Json j = new Json();
		try {
			String kaptcha = (String)request.getSession().getAttribute("rand");
			if(form.getKaptcha() != null && !form.getKaptcha().equalsIgnoreCase(kaptcha)) {
				j.setMsg("验证码错误！");
				return j ;
			}
			
			UserForm user = this.empService.login(form);
			if (user != null) {
				if(0 == user.getStatus()) {
					request.getSession().removeAttribute("rand");
					
					user.setIp(IpUtil.getIpAddr(request)) ;
					LoginInfoSession LoginInfo = new LoginInfoSession();
					LoginInfo.setUser(user) ;
					LoginInfo.setResourceList(this.empService.MyPermission(user.getId(), user.getAccount()));
					request.getSession().setAttribute(Constants.SESSION_INFO_NAME, LoginInfo);
					
					j.setStatus(true);
					j.setMsg("登陆成功！");
					j.setObj(LoginInfo);
				} else {
					j.setStatus(false);
					j.setMsg("账号未激活,请与管理员联系!");
				}
			} else {
				j.setStatus(false);
				j.setMsg("用户名或密码错误！");
			}
			
			return j ;
		} catch (Exception e) {
			throw new Exception("发生错误：" + e.getMessage()) ;
		}
	}
	
	/**
	 * 修改我的密码
	 * @param session
	 * @param oldPwd
	 * @param pwd
	 */
	@RequestMapping("/doNotNeedAuth_editMyPwd.do")
	@ResponseBody
	public Json doNotNeedAuth_editMyPwd(UserForm user, HttpServletRequest request) {
		Json j = new Json();
		try {
			if (request.getSession() != null) {
				LoginInfoSession sessionInfo = (LoginInfoSession) request.getSession().getAttribute(Constants.SESSION_INFO_NAME);
				if (sessionInfo != null) {
					if (this.empService.editCurrentUserPwd(sessionInfo, user.getOldPwd(), user.getPassword())) {
						j.setStatus(true);
						j.setMsg("编辑密码成功，下次登录生效！");
					} else {
						j.setStatus(false);
						j.setMsg("原密码错误！");
					}
				} else {
					j.setStatus(false);
					j.setMsg("登录超时，请重新登录！");
				}
			} else {
				j.setStatus(false);
				j.setMsg("登录超时，请重新登录！");
			}
			
		} catch (Exception e) {
			j.setStatus(false);
			j.setMsg("修改我的密码失败。<br>" + e.getMessage());
		}
		
		return j ;
	}
	
	/**
	 * 注销登录
	 */
	@RequestMapping("/doNotNeedSession_logout.do")
	@ResponseBody
	public Json doNotNeedSession_logout(HttpServletRequest request) {
		Json j = new Json();
		if (request.getSession() != null) {
			request.getSession().invalidate();
			j.setStatus(true);
		} else {
			j.setStatus(false);
		}
		return j ;
	}
	
}
