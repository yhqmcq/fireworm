package com.galaxy.system.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.service.ModuleServiceI;
import com.galaxy.system.web.form.ActionForm;
import com.galaxy.system.web.form.ModuleForm;

@Controller
@RequestMapping("/sysmgr/moduleAction")
public class ModuleAction extends BaseController {
	
	@Autowired
	private ModuleServiceI moduleService ;
	
	@RequestMapping("/module_main.do")
	public String module_main() {
		return Constants.SYSMGR + "module_main" ;
	}
	
	@RequestMapping("/module_permit.do")
	public String module_permit() {
		return Constants.SYSMGR + "module_permit" ;
	}
	
	@RequestMapping("/module_form.do")
	public String module_form(ModuleForm form, HttpServletRequest request) {
		if(null != form.getId() && !"".equals(form.getId())) {
			request.setAttribute("id", form.getId()) ;
		}
		return Constants.SYSMGR + "module_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public ModuleForm get(ModuleForm form, HttpServletRequest request) {
		return this.moduleService.get(form.getId()) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	public Json add(ModuleForm form, HttpServletRequest request) {
		Json j = this.moduleService.save(form) ;
		this.export_menu(request) ;
		return j ; 
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(ModuleForm form, HttpServletRequest request) {
		Json j = this.moduleService.delete(form.getId()) ;
		this.export_menu(request) ;
		return j ; 
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(ModuleForm form, HttpServletRequest request) {
		Json j = this.moduleService.edit(form) ;
		this.export_menu(request) ;
		return j ; 
	}
	
	@RequestMapping("/ondrop.do")
	@ResponseBody
	public Json ondrop(ModuleForm form, HttpServletRequest request) {
		Json j = this.moduleService.ondrop(form) ;
		this.export_menu(request) ;
		return j ; 
	}
	
	@RequestMapping("/treegrid.do")
	@ResponseBody
	public List<ModuleForm> treegrid(ModuleForm form, HttpServletRequest request) {
		return this.moduleService.findMenusAll(form, false) ;
	}
	
	@RequestMapping("/doNotNeedAuth_combox.do")
	@ResponseBody
	public List<ModuleForm> doNotNeedAuth_combox(ModuleForm form, HttpServletRequest request) {
		return this.moduleService.findMenusAll(form, true) ;
	}
	
	@RequestMapping("/rece_permit.do")
	@ResponseBody
	public List<ModuleForm> rece_permit(ModuleForm form, HttpServletRequest request) {
		return this.moduleService.findMenusAll(form, true) ;
	}
	
	@RequestMapping("/export_menu.do")
	public void export_menu(HttpServletRequest request) {
		this.moduleService.exportMenusAll(request.getServletContext()) ;
	}

	@RequestMapping("/doNotNeedAuth_viewaction.do")
	@ResponseBody
	public List<ActionForm> doNotNeedAuth_viewaction(ActionForm form) {
		return this.moduleService.view_action(form) ;
	}
	
}
