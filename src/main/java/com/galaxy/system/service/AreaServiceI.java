package com.galaxy.system.service;

import java.util.List;

import com.galaxy.system.web.form.AreaForm;

public interface AreaServiceI {

	//递归查询
	public List<AreaForm> findAreas(AreaForm form) ;
	
	//查询省  
	public List<AreaForm> findProvince(AreaForm form) ;
	
	//查询市  
	public List<AreaForm> findCity(AreaForm form) ;
	
	//查询县区  
	public List<AreaForm> findCounty(AreaForm form) ;
	
}
