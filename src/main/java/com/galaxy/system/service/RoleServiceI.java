package com.galaxy.system.service;

import java.util.List;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.web.form.RoleForm;

public interface RoleServiceI {
	
	public Json save(RoleForm form) ;
	
	public Json delete(RoleForm form) ;
	
	public Json edit(RoleForm form) ;
	
	public RoleForm get(RoleForm form) ;
	
	public DataGrid datagrid(RoleForm form) ;
	
	public List<RoleForm> treegrid(RoleForm form) ;

	public Json set_grant(RoleForm form) ;
	
	public RoleForm getPermission(RoleForm form) ;
	
}
