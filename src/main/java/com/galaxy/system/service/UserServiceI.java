package com.galaxy.system.service;

import java.util.List;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.common.web.page.LoginInfoSession;
import com.galaxy.system.web.form.UserForm;

public interface UserServiceI {

	public Json save(UserForm form) ;
	
	public Json addOrUpdate(UserForm form) ;
	
	public Json delete(UserForm form) ;
	
	public Json edit(UserForm form) ;
	
	public UserForm get(String id) ;
	
	public UserForm get(UserForm form) ;
	
	public DataGrid datagrid(UserForm form) ;
	
	public Json set_permit(UserForm form) ;
	
	public Json set_module_permit(UserForm form) ;
	
	public UserForm getPermission(UserForm form) ;
	
	public UserForm get_module_permit(UserForm form) ;
	
	public UserForm login(UserForm user) ;
	
	public List<String> MyPermission(String id, String username) throws Exception ;
	
	public boolean editCurrentUserPwd(LoginInfoSession sessionInfo, String oldPwd, String pwd) ;
}
