package com.galaxy.system.service;

import java.util.List;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.entity.TaskEntity;
import com.galaxy.system.web.form.TaskForm;

public interface TaskSchedulerServiceI {

	public Json save(TaskForm form) ;
	
	public Json delete(TaskForm form) ;
	
	public Json edit(TaskForm form) ;
	
	public TaskForm get(String id) ;
	
	public DataGrid datagrid(TaskForm form) ;
	
	public List<TaskEntity> find(TaskForm form) ;
	
}
