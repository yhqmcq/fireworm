package com.galaxy.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.galaxy.common.dao.BaseDaoI;
import com.galaxy.common.util.BeanUtils;
import com.galaxy.system.entity.AreaEntity;
import com.galaxy.system.service.AreaServiceI;
import com.galaxy.system.web.form.AreaForm;

@Service
@Transactional
public class AreaServiceImpl implements AreaServiceI {

	@Autowired
	private BaseDaoI<AreaEntity> basedaoArea;

	@Transactional(readOnly = true)
	@Override
	public List<AreaForm> findAreas(AreaForm form) {
		List<AreaForm> forms = new ArrayList<AreaForm>();
		
		String hql = "select t from AreaEntity t where t.parentAreaId='"+(null != form.getId() && !"".equals(form.getId())?form.getId():0)+"'" ;
		List<AreaEntity> list = this.basedaoArea.find(hql) ;
		for (AreaEntity entity : list) {
			AreaForm f = new AreaForm() ;
			BeanUtils.copyProperties(entity, f);
			forms.add(f) ;
		}
		
		return forms;
	}

	@Transactional(readOnly = true)
	@Override
	public List<AreaForm> findProvince(AreaForm form) {
		List<AreaForm> forms = new ArrayList<AreaForm>();
		
		String hql = "select t from AreaEntity t where t.parentAreaId='0'" ;
		List<AreaEntity> list = this.basedaoArea.find(hql) ;
		for (AreaEntity entity : list) {
			AreaForm f = new AreaForm() ;
			BeanUtils.copyProperties(entity, f);
			forms.add(f) ;
		}
		
		return forms;
	}

	@Transactional(readOnly = true)
	@Override
	public List<AreaForm> findCity(AreaForm form) {
		List<AreaForm> forms = new ArrayList<AreaForm>();

		return forms;
	}

	@Transactional(readOnly = true)
	@Override
	public List<AreaForm> findCounty(AreaForm form) {
		List<AreaForm> forms = new ArrayList<AreaForm>();

		return forms;
	}

}
