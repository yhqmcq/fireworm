package com.galaxy.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.galaxy.common.dao.BaseDaoI;
import com.galaxy.common.quartz.SchedulerUtil;
import com.galaxy.common.util.string.UUIDHexGenerator;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.system.entity.TaskEntity;
import com.galaxy.system.service.TaskSchedulerServiceI;
import com.galaxy.system.web.form.TaskForm;

@Service
@Transactional
public class TaskSchedulerServiceImpl implements TaskSchedulerServiceI {

	@Autowired
	private BaseDaoI<TaskEntity> basedaoTask ;
	
	@Autowired
	private SchedulerUtil schedulerUtil ;

	@Override
	public Json save(TaskForm form) {
		Json j = new Json();
		try {
			String id = UUIDHexGenerator.generator() ;
			if(null == form.getTask_code() || form.getTask_code().equals("")) {
				form.setTask_code(id) ;
			}
			this.schedulerUtil.scheduler(form) ;
			
			TaskEntity entity = new TaskEntity() ;
			BeanUtils.copyProperties(form, entity) ;
			entity.setId(id) ;
			this.basedaoTask.save(entity) ;
			j.setMsg("添加成功！");
			j.setStatus(true);
		} catch (Exception e) {
			j.setMsg("添加失败！");
		}
		return j;
	}

	@Override
	public Json delete(TaskForm form) {
		Json j = new Json();
		try {
			if(null != form.getIds() && !form.getIds().equalsIgnoreCase("")) {
				String[] id = form.getIds().split(",") ;
				for(int i=0;i<id.length;i++) {
					TaskEntity entity = this.basedaoTask.get(TaskEntity.class, id) ;
					if(null != entity) {
						TaskForm task = new TaskForm() ;
						BeanUtils.copyProperties(entity, task) ;
						this.schedulerUtil.deleteJob(task) ;
					}
					this.basedaoTask.delete(entity);
				}
			}
			j.setMsg("删除成功！");
			j.setStatus(true);
		} catch (Exception e) {
			j.setMsg("删除失败！");
		}
		return j;
	}

	@Override
	public Json edit(TaskForm form) {
		Json j = new Json();
		try {
			String currentEnable = null, cron_expression = null ;
			
			TaskEntity entity = this.basedaoTask.get(TaskEntity.class, form.getId());
			currentEnable = entity.getTask_enable() ; cron_expression = entity.getCron_expression() ;
			BeanUtils.copyProperties(form, entity);
			this.basedaoTask.update(entity);
			
			//判断任务修改前的状态he修改后的状态是否不相同，不相同则修改任务的状态
			if(!form.getTask_enable().equalsIgnoreCase(currentEnable)) {
				TaskForm task = new TaskForm() ;
				BeanUtils.copyProperties(entity, task) ;
				if("Y".equalsIgnoreCase(form.getTask_enable())) {
					this.schedulerUtil.resumeJob(task) ;
				} else {
					this.schedulerUtil.pauseJob(task) ;
				}
			}
			//判断任务修改前和修改后的触发时间是否一致，不一致则重新设定触发时间
			if(!form.getCron_expression().equalsIgnoreCase(cron_expression)) {
				//不管任务的状态是否启动，都设为启动
				entity.setTask_enable("Y") ;
				TaskForm task = new TaskForm() ;
				BeanUtils.copyProperties(entity, task) ;
				this.schedulerUtil.rescheduleJob(task) ;
			}
			j.setMsg("编辑成功！");
			j.setStatus(true);
		} catch (Exception e) {
			j.setMsg("编辑失败！");
		}
		return j;
	}

	@Override
	public TaskForm get(String id) {
		TaskForm form = new TaskForm();
		TaskEntity entity = this.basedaoTask.get(TaskEntity.class, id);
		BeanUtils.copyProperties(entity, form);
		return form;
	}

	@Override
	public DataGrid datagrid(TaskForm form) {
		DataGrid datagrid = new DataGrid();
		datagrid.setTotal(this.total(form));
		datagrid.setRows(this.changeModel(this.find(form)));
		return datagrid;
	}
	
	
	private List<TaskForm> changeModel(List<TaskEntity> TaskEntity) {
		List<TaskForm> forms = new ArrayList<TaskForm>();

		if (null != TaskEntity && TaskEntity.size() > 0) {
			for (TaskEntity i : TaskEntity) {
				TaskForm uf = new TaskForm();
				BeanUtils.copyProperties(i, uf);
				forms.add(uf);
			}
		}
		return forms;
	}

	public List<TaskEntity> find(TaskForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from TaskEntity t where 1=1";
		hql = addWhere(hql, form, params) + addOrdeby(form);
		return this.basedaoTask.find(hql, params, form.getPage(), form.getRows());
	}

	private String addOrdeby(TaskForm form) {
		String orderString = "";
		if (form.getSort() != null && form.getOrder() != null) {
			orderString = " order by " + form.getSort() + " " + form.getOrder();
		}
		return orderString;
	}

	public Long total(TaskForm form) {
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = "select count(*) from TaskEntity t where 1=1";

		hql = addWhere(hql, form, params);

		return this.basedaoTask.count(hql, params);
	}

	private String addWhere(String hql, TaskForm form, Map<String, Object> params) {
		if (null != form) {
			if(form.getTask_enable() != null && !form.getTask_enable().equals("")) {
				hql += " and t.task_enable=:task_enable";
				params.put("task_enable", form.getTask_enable());
			}
		}
		return hql;
	}
	
}
