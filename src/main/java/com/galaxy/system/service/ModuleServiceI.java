package com.galaxy.system.service;

import java.util.List;

import javax.servlet.ServletContext;

import com.galaxy.common.web.page.Json;
import com.galaxy.system.web.form.ActionForm;
import com.galaxy.system.web.form.ModuleForm;

public interface ModuleServiceI {

	public Json save(ModuleForm form) ;
	
	public Json saveOrupdate(ModuleForm form) ;
	
	public Json delete(String id) ;
	
	public Json edit(ModuleForm form) ;
	
	public Json ondrop(ModuleForm form) ;
	
	public ModuleForm get(String id) ;
	
	public List<ModuleForm> findMenusAll(ModuleForm form, boolean flag) ;
	
	public void exportMenusAll(ServletContext sc) ;
	
	public Json save_action(ActionForm form) ;
	
	public ActionForm get_action(ActionForm form) ;
	
	public Json delete_action(ActionForm form) ;
	
	public Json edit_action(ActionForm form) ;
	
	public List<ActionForm> view_action(ActionForm form) ;
}
