package com.galaxy.system.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.galaxy.common.util.number.RandomUtils;

/**
 * 系统模块表（资源表）
 * 创建者： 杨浩泉
 * 创建时间： 2014-3-17 下午9:21:03
 * 版本号： v1.0
 */
@Entity
@Table(name = "GALAXY_SYSTEM_MODULE")
@DynamicUpdate(true)
@DynamicInsert(true)
public class ModuleEntity {

	private String id ;
	
	/** 模块名称 */
	private String moduleName ;
	
	/** 模块值(sys_user) */
	private String moduleValue ;
	
	/** 资源链接地址 */
	private String linkUrl ;
	
	private String remark ;
	
	/** 类型（类别、菜单、操作） */
	private String type ;
	
	/** 排序 */
	private Integer seq ;
	
	/** 是否启用 */
	private String disused ;
	
	/** 菜单图标 */
	private String iconCls ;
	
	/** tree{open,closed} */
	private String state ;

	private Date created = new Date() ;
	
	private Set<ModuleEntity> modules = new HashSet<ModuleEntity>() ;
	
	private ModuleEntity module ;
	
	private Set<RoleEntity> role_permit = new HashSet<RoleEntity>(0) ;
	
	private Set<UserEntity> user_permit = new HashSet<UserEntity>(0) ;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "GALAXY_SYSTEM_USER_MODULE_PERMIT", joinColumns = { @JoinColumn(name = "MODULE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) })
	public Set<UserEntity> getUser_permit() {
		return user_permit;
	}

	public void setUser_permit(Set<UserEntity> user_permit) {
		this.user_permit = user_permit;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "GALAXY_SYSTEM_ROLE_MODULE_PERMIT", joinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "MODULE_ID", nullable = false, updatable = false) })
	public Set<RoleEntity> getRole_permit() {
		return role_permit;
	}

	public void setRole_permit(Set<RoleEntity> role_permit) {
		this.role_permit = role_permit;
	}

	@OneToMany
	@JoinColumn(name="MODULE_PID")
	@OrderBy("seq desc")
	public Set<ModuleEntity> getModules() {
		return modules;
	}

	public void setModules(Set<ModuleEntity> modules) {
		this.modules = modules;
	}

	@ManyToOne
	@JoinColumn(name="MODULE_PID")
	public ModuleEntity getModule() {
		return module;
	}

	public void setModule(ModuleEntity module) {
		this.module = module;
	}

	
	public Date getCreated() {
		return created;
	}

	public String getDisused() {
		return disused;
	}

	public String getIconCls() {
		return iconCls;
	}

	@Id
	public String getId() {
		if (this.id != null && !"".equals(this.id)) {
			return this.id;
		}
		return RandomUtils.generateNumber(6);
	}

	public String getLinkUrl() {
		return linkUrl;
	}
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getModuleName() {
		return moduleName;
	}

	public String getModuleValue() {
		return moduleValue;
	}

	public Integer getSeq() {
		return seq;
	}

	public String getState() {
		return state;
	}

	public String getType() {
		return type;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setDisused(String disused) {
		this.disused = disused;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setModuleValue(String moduleValue) {
		this.moduleValue = moduleValue;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
