package com.galaxy.fireworm.entity;

import java.sql.Clob;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.galaxy.common.util.number.RandomUtils;

/**
 * 类描述：儿童信息表 
 * 创建时间： 2014-4-30 下午6:10:49
 * 创建者： 杨浩泉
 * 版本号： v1.0
 */
@Entity
@Table(name = "GALAXY_FIREWORM_CHILDREN")
@DynamicUpdate(true)
@DynamicInsert(true)
public class ChildrenEntity {

private String id ;
	
	private String name ;
	
	private String sname ;
	
	private String sex ;
	
	private Date brith ;						//出生日期
	
	private Date lost_time ;					//丢失事件
	
	private Float lost_height ;					//丢前身高
	
	private String isPay = "否" ;				//是否抽血
	
	private Float money = new Float(0) ;		//抽血金额
	
	private String province ;
	
	private String city ;
	
	private String county ;
	
	private String lost_addr ;
	
	private Clob photos_path ;					//图片名称，多张使用逗号“，”分割
	
	private Clob description;					//描述
	
	private Date created = new Date() ;
	
	private PatriarchEntity patriarch ;
	
	@ManyToOne @JoinColumn(name="PATRIARCH_ID")
	public PatriarchEntity getPatriarch() {
		return patriarch;
	}

	public void setPatriarch(PatriarchEntity patriarch) {
		this.patriarch = patriarch;
	}

	@Id
	public String getId() {
		if (this.id != null && !"".equals(this.id)) {
			return this.id;
		}
		return RandomUtils.generateNumber(6);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLost_addr() {
		return lost_addr;
	}

	public String getIsPay() {
		return isPay;
	}

	public void setIsPay(String isPay) {
		this.isPay = isPay;
	}

	public Float getMoney() {
		return money;
	}

	public void setMoney(Float money) {
		this.money = money;
	}

	public void setLost_addr(String lost_addr) {
		this.lost_addr = lost_addr;
	}

	public String getSex() {
		return sex;
	}


	public Clob getPhotos_path() {
		return photos_path;
	}

	public void setPhotos_path(Clob photos_path) {
		this.photos_path = photos_path;
	}

	public Clob getDescription() {
		return description;
	}

	public void setDescription(Clob description) {
		this.description = description;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Date getBrith() {
		return brith;
	}

	public void setBrith(Date brith) {
		this.brith = brith;
	}

	public Date getLost_time() {
		return lost_time;
	}

	public void setLost_time(Date lost_time) {
		this.lost_time = lost_time;
	}

	public Float getLost_height() {
		return lost_height;
	}

	public void setLost_height(Float lost_height) {
		this.lost_height = lost_height;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}


	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	
}
