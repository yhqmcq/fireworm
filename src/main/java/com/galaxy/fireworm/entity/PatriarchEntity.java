package com.galaxy.fireworm.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.galaxy.common.util.number.RandomUtils;

/**
 * 类描述：家长信息表 
 * 创建时间： 2014-4-30 下午6:10:20
 * 创建者： 杨浩泉
 * 版本号： v1.0
 */
@Entity
@Table(name = "GALAXY_FIREWORM_PATRIARCH")
@DynamicUpdate(true)
@DynamicInsert(true)
public class PatriarchEntity {

	private String id ;
	
	private String name ;
	
	private String sex ;
	
	private String mobile ;			//手机作为登录账号
	
	private String email ;			//邮箱地址
	
	private String password ;		//登录密码
	
	private String remark ;			//备注
	
	private String addr ;			//联系地址
	
	private Date updateDateTime ; 	//刷新时间，以这个来作为排序
	
	private Date created = new Date() ;

	private Set<ChildrenEntity> childrens = new HashSet<ChildrenEntity>(0) ;
	
	@OneToMany(mappedBy="patriarch", fetch=FetchType.LAZY)
	@OrderBy("created desc")
	public Set<ChildrenEntity> getChildrens() {
		return childrens;
	}

	public void setChildrens(Set<ChildrenEntity> childrens) {
		this.childrens = childrens;
	}

	@Id
	public String getId() {
		if (this.id != null && !"".equals(this.id)) {
			return this.id;
		}
		return RandomUtils.generateNumber(6);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
}
