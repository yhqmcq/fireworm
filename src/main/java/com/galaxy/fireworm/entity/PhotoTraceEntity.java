package com.galaxy.fireworm.entity;

import java.sql.Clob;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.galaxy.common.util.number.RandomUtils;

/**
 * 类描述：拍照定位追踪表 
 * 创建时间： 2014-4-30 下午6:10:49
 * 创建者： 杨浩泉
 * 版本号： v1.0
 */
@Entity
@Table(name = "GALAXY_FIREWORM_PHOTOTRACE")
@DynamicUpdate(true)
@DynamicInsert(true)
public class PhotoTraceEntity {

	private String id ;
	
	private String name ;			//图片来源者姓名
	
	private String mobile ;			//图片来源者联系电话
	
	private String photo_origin ;	//图片来源（PC、Mobile）
	
	private String photo_locale ;	//图片拍摄地点
	
	private Clob photos_path ;	//图片地址
	
	private Date created = new Date() ;
	
	@Id
	public String getId() {
		if (this.id != null && !"".equals(this.id)) {
			return this.id;
		}
		return RandomUtils.generateString(32);
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhoto_origin() {
		return photo_origin;
	}

	public void setPhoto_origin(String photo_origin) {
		this.photo_origin = photo_origin;
	}


	public Clob getPhotos_path() {
		return photos_path;
	}

	public void setPhotos_path(Clob photos_path) {
		this.photos_path = photos_path;
	}

	public String getPhoto_locale() {
		return photo_locale;
	}

	public void setPhoto_locale(String photo_locale) {
		this.photo_locale = photo_locale;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	
}
