package com.galaxy.fireworm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.galaxy.common.dao.BaseDaoI;
import com.galaxy.common.util.BeanUtils;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.entity.PatriarchEntity;
import com.galaxy.fireworm.service.PatriarchServiceI;
import com.galaxy.fireworm.web.form.PatriarchForm;

@Service
@Transactional
public class PatriarchServiceImpl implements PatriarchServiceI {

	@Autowired
	private BaseDaoI<PatriarchEntity> basedaoPatriarch;

	@Override
	public Json save(PatriarchForm form) {
		Json j = new Json();

		PatriarchEntity entity = new PatriarchEntity() ;
		BeanUtils.copyProperties(form, entity, new String[]{"created"}) ;
		this.basedaoPatriarch.save(entity) ;
		
		j.setMsg("添加成功！") ;
		j.setStatus(true) ;
		return j;
	}

	@Override
	public Json delete(PatriarchForm form) {
		Json j = new Json();
		try {
			if(null != form.getIds() && !"".equals(form.getIds())) {
				String[] ids = form.getIds().split(",") ;
				for (String id : ids) {
					PatriarchEntity o = this.basedaoPatriarch.get(PatriarchEntity.class, id);
					this.basedaoPatriarch.delete(o) ;
				}
				j.setMsg("删除成功！") ;
				j.setStatus(true) ;
			}
		} catch (BeansException e) {
			j.setMsg("删除失败！") ;
		}
		return j;
	}

	@Override
	public Json edit(PatriarchForm form) {
		Json j = new Json();
		try {
			PatriarchEntity entity = this.basedaoPatriarch.get(PatriarchEntity.class, form.getId());
			
			BeanUtils.copyProperties(form, entity, new String[]{"created"});
			
			this.basedaoPatriarch.update(entity) ;
			
			j.setMsg("编辑成功！") ;
			j.setStatus(true) ;
		} catch (BeansException e) {
			j.setMsg("编辑失败！") ;
		}
		return j;
	}

	@Override
	public PatriarchForm get(PatriarchForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from PatriarchEntity t where 1=1";
		hql = addWhere(hql, form, params);
		
		PatriarchEntity entity = this.basedaoPatriarch.get(hql, params) ;
		if(null != entity) {
			PatriarchForm pform = new PatriarchForm();
			BeanUtils.copyProperties(entity, pform);
			return pform ;
		} else {
			return null ;
		}
	}

	@Override
	public DataGrid datagrid(PatriarchForm form) {
		
		List<PatriarchForm> forms = new ArrayList<PatriarchForm>();
		List<PatriarchEntity> entitys = this.find(form) ;
		for (PatriarchEntity entity : entitys) {
			PatriarchForm f = new PatriarchForm() ;
			BeanUtils.copyProperties(entity, f) ;
			forms.add(f) ;
		}
		
		DataGrid datagrid = new DataGrid();
		datagrid.setTotal(this.total(form));
		datagrid.setRows(forms);
		return datagrid;
	}
	
	private List<PatriarchEntity> find(PatriarchForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from PatriarchEntity t where 1=1";
		hql = addWhere(hql, form, params) + addOrdeby(form);
		return this.basedaoPatriarch.find(hql, params, form.getPage(), form.getRows());
	}

	private String addOrdeby(PatriarchForm form) {
		String orderString = "";
		if (form.getSort() != null && form.getOrder() != null) {
			orderString = " order by " + form.getSort() + " " + form.getOrder();
		}
		return orderString;
	}

	public Long total(PatriarchForm form) {
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = "select count(*) from PatriarchEntity t where 1=1";

		hql = addWhere(hql, form, params);

		return this.basedaoPatriarch.count(hql, params);
	}

	private String addWhere(String hql, PatriarchForm form, Map<String, Object> params) {
		if (null != form) {
			/*if (form.getTruename() != null && !"".equals(form.getTruename())) {
				hql += " and t.name like :name";
				params.put("name", "%%" + form.getTruename() + "%%");
			}
			if (form.getAccount() != null && !"".equals(form.getAccount())) {
				hql += " and t.account like :account";
				params.put("account", "%%" + form.getAccount() + "%%");
			}
			if (form.getSex() != null && !"".equals(form.getSex())) {
				hql += " and t.sex like :sex";
				params.put("sex", "%%" + form.getSex() + "%%");
			}
			if (form.getTel() != null && !"".equals(form.getTel())) {
				hql += " and t.tel like :tel";
				params.put("tel", "%%" + form.getTel() + "%%");
			}
			if (form.getEmail() != null && !"".equals(form.getEmail())) {
				hql += " and t.email like :email";
				params.put("email", "%%" + form.getEmail() + "%%");
			}
			if (form.getOrgname() != null && !"".equals(form.getOrgname())) {
				hql += " and t.orgname like :orgname";
				params.put("orgname", "%%" + form.getOrgname() + "%%");
			}
			if (form.getOnlineState() != null && !"".equals(form.getOrgname())) {
				hql += " and t.onlineState=:onlineState";
				params.put("onlineState", form.getOnlineState());
			}*/
		}
		return hql;
	}

}
