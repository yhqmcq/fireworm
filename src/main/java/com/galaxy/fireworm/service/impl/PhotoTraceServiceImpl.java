package com.galaxy.fireworm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.galaxy.common.dao.BaseDaoI;
import com.galaxy.common.util.BeanUtils;
import com.galaxy.common.util.string.ClobUtil;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.entity.PhotoTraceEntity;
import com.galaxy.fireworm.service.PhotoTraceServiceI;
import com.galaxy.fireworm.web.form.PhotoTraceForm;

@Service
@Transactional
public class PhotoTraceServiceImpl implements PhotoTraceServiceI {

	@Autowired
	private BaseDaoI<PhotoTraceEntity> basedaoPhotoTrace;

	@Override
	public Json save(PhotoTraceForm form) {
		Json j = new Json();

		PhotoTraceEntity entity = new PhotoTraceEntity() ;
		BeanUtils.copyProperties(form, entity, new String[]{"created", "photos_path"}) ;
		entity.setPhotos_path(ClobUtil.getClob(form.getPhotos_path())) ;
		this.basedaoPhotoTrace.save(entity) ;
		
		j.setMsg("添加成功！") ;
		j.setStatus(true) ;
		return j;
	}

	@Override
	public Json delete(PhotoTraceForm form) {
		Json j = new Json();
		try {
			if(null != form.getIds() && !"".equals(form.getIds())) {
				String[] ids = form.getIds().split(",") ;
				for (String id : ids) {
					PhotoTraceEntity o = this.basedaoPhotoTrace.get(PhotoTraceEntity.class, id);
					this.basedaoPhotoTrace.delete(o) ;
				}
				j.setMsg("删除成功！") ;
				j.setStatus(true) ;
			}
		} catch (BeansException e) {
			j.setMsg("删除失败！") ;
		}
		return j;
	}

	@Override
	public Json edit(PhotoTraceForm form) {
		Json j = new Json();
		try {
			PhotoTraceEntity entity = this.basedaoPhotoTrace.get(PhotoTraceEntity.class, form.getId());
			
			BeanUtils.copyProperties(form, entity, new String[]{"created", "photos_path"}) ;
			entity.setPhotos_path(ClobUtil.getClob(form.getPhotos_path())) ;
			
			this.basedaoPhotoTrace.update(entity) ;
			
			j.setMsg("编辑成功！") ;
			j.setStatus(true) ;
		} catch (BeansException e) {
			j.setMsg("编辑失败！") ;
		}
		return j;
	}

	@Override
	public PhotoTraceForm get(PhotoTraceForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from PhotoTraceEntity t where 1=1";
		hql = addWhere(hql, form, params);
		
		PhotoTraceEntity entity = this.basedaoPhotoTrace.get(hql, params) ;
		if(null != entity) {
			PhotoTraceForm pform = new PhotoTraceForm();
			BeanUtils.copyProperties(entity, pform, new String[]{"photos_path"}) ;
			pform.setPhotos_path(ClobUtil.getString(entity.getPhotos_path())) ;
			return pform ;
		} else {
			return null ;
		}
	}

	@Override
	public DataGrid datagrid(PhotoTraceForm form) {
		List<PhotoTraceForm> forms = new ArrayList<PhotoTraceForm>() ;
		List<PhotoTraceEntity> entitys = this.find(form) ;
		for (PhotoTraceEntity entity : entitys) {
			PhotoTraceForm f = new PhotoTraceForm() ;
			BeanUtils.copyProperties(entity, f, new String[]{"photos_path"}) ;
			f.setPhotos_path(ClobUtil.getString(entity.getPhotos_path())) ;
			forms.add(f) ;
		}
		DataGrid datagrid = new DataGrid();
		datagrid.setTotal(this.total(form));
		datagrid.setRows(forms);
		return datagrid;
	}
	
	private List<PhotoTraceEntity> find(PhotoTraceForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from PhotoTraceEntity t where 1=1";
		hql = addWhere(hql, form, params) + addOrdeby(form);
		return this.basedaoPhotoTrace.find(hql, params, form.getPage(), form.getRows());
	}

	private String addOrdeby(PhotoTraceForm form) {
		String orderString = "";
		if (form.getSort() != null && form.getOrder() != null) {
			orderString = " order by " + form.getSort() + " " + form.getOrder();
		}
		return orderString;
	}

	public Long total(PhotoTraceForm form) {
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = "select count(*) from PhotoTraceEntity t where 1=1";

		hql = addWhere(hql, form, params);

		return this.basedaoPhotoTrace.count(hql, params);
	}

	private String addWhere(String hql, PhotoTraceForm form, Map<String, Object> params) {
		if (null != form) {
			/*if (form.getTruename() != null && !"".equals(form.getTruename())) {
				hql += " and t.name like :name";
				params.put("name", "%%" + form.getTruename() + "%%");
			}
			if (form.getAccount() != null && !"".equals(form.getAccount())) {
				hql += " and t.account like :account";
				params.put("account", "%%" + form.getAccount() + "%%");
			}
			if (form.getSex() != null && !"".equals(form.getSex())) {
				hql += " and t.sex like :sex";
				params.put("sex", "%%" + form.getSex() + "%%");
			}
			if (form.getTel() != null && !"".equals(form.getTel())) {
				hql += " and t.tel like :tel";
				params.put("tel", "%%" + form.getTel() + "%%");
			}
			if (form.getEmail() != null && !"".equals(form.getEmail())) {
				hql += " and t.email like :email";
				params.put("email", "%%" + form.getEmail() + "%%");
			}
			if (form.getOrgname() != null && !"".equals(form.getOrgname())) {
				hql += " and t.orgname like :orgname";
				params.put("orgname", "%%" + form.getOrgname() + "%%");
			}
			if (form.getOnlineState() != null && !"".equals(form.getOrgname())) {
				hql += " and t.onlineState=:onlineState";
				params.put("onlineState", form.getOnlineState());
			}*/
		}
		return hql;
	}

}
