package com.galaxy.fireworm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.galaxy.common.dao.BaseDaoI;
import com.galaxy.common.util.BeanUtils;
import com.galaxy.common.util.string.ClobUtil;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.entity.ChildrenEntity;
import com.galaxy.fireworm.service.ChildrenServiceI;
import com.galaxy.fireworm.web.form.ChildrenForm;

@Service
@Transactional
public class ChildrenServiceImpl implements ChildrenServiceI {

	@Autowired
	private BaseDaoI<ChildrenEntity> basedaoChildren;

	@Override
	public Json save(ChildrenForm form) {
		Json j = new Json();

		ChildrenEntity entity = new ChildrenEntity() ;
		BeanUtils.copyProperties(form, entity, new String[]{"created", "photos_path", "description"}) ;
		entity.setDescription(ClobUtil.getClob(form.getDescription())) ;
		entity.setPhotos_path(ClobUtil.getClob(form.getPhotos_path())) ;
		this.basedaoChildren.save(entity) ;
		
		j.setMsg("添加成功！") ;
		j.setStatus(true) ;
		return j;
	}

	@Override
	public Json delete(ChildrenForm form) {
		Json j = new Json();
		try {
			if(null != form.getIds() && !"".equals(form.getIds())) {
				String[] ids = form.getIds().split(",") ;
				for (String id : ids) {
					ChildrenEntity o = this.basedaoChildren.get(ChildrenEntity.class, id);
					this.basedaoChildren.delete(o) ;
				}
				j.setMsg("删除成功！") ;
				j.setStatus(true) ;
			}
		} catch (BeansException e) {
			j.setMsg("删除失败！") ;
		}
		return j;
	}

	@Override
	public Json edit(ChildrenForm form) {
		Json j = new Json();
		try {
			ChildrenEntity entity = this.basedaoChildren.get(ChildrenEntity.class, form.getId());
			
			BeanUtils.copyProperties(form, entity, new String[]{"created", "photos_path", "description"}) ;
			entity.setDescription(ClobUtil.getClob(form.getDescription())) ;
			entity.setPhotos_path(ClobUtil.getClob(form.getPhotos_path())) ;
			
			this.basedaoChildren.update(entity) ;
			
			j.setMsg("编辑成功！") ;
			j.setStatus(true) ;
		} catch (BeansException e) {
			j.setMsg("编辑失败！") ;
		}
		return j;
	}

	@Override
	public ChildrenForm get(ChildrenForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from ChildrenEntity t where 1=1";
		hql = addWhere(hql, form, params);
		
		ChildrenEntity entity = this.basedaoChildren.get(hql, params) ;
		if(null != entity) {
			ChildrenForm pform = new ChildrenForm();
			BeanUtils.copyProperties(entity, pform, new String[]{"photos_path", "description"});
			pform.setPhotos_path(ClobUtil.getString(entity.getPhotos_path())) ;
			pform.setDescription(ClobUtil.getString(entity.getDescription())) ;
			return pform ;
		} else {
			return null ;
		}
	}

	@Override
	public DataGrid datagrid(ChildrenForm form) {
		List<ChildrenForm> forms = new ArrayList<ChildrenForm>() ;
		List<ChildrenEntity> entitys = this.find(form) ;
		for (ChildrenEntity entity : entitys) {
			ChildrenForm f = new ChildrenForm() ;
			BeanUtils.copyProperties(entity, f, new String[]{"description"}) ;
			forms.add(f) ;
		}
		DataGrid datagrid = new DataGrid();
		datagrid.setTotal(this.total(form));
		datagrid.setRows(forms);
		return datagrid;
	}
	
	private List<ChildrenEntity> find(ChildrenForm form) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "select t from ChildrenEntity t where 1=1";
		hql = addWhere(hql, form, params) + addOrdeby(form);
		return this.basedaoChildren.find(hql, params, form.getPage(), form.getRows());
	}

	private String addOrdeby(ChildrenForm form) {
		String orderString = "";
		if (form.getSort() != null && form.getOrder() != null) {
			orderString = " order by " + form.getSort() + " " + form.getOrder();
		}
		return orderString;
	}

	public Long total(ChildrenForm form) {
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = "select count(*) from ChildrenEntity t where 1=1";

		hql = addWhere(hql, form, params);

		return this.basedaoChildren.count(hql, params);
	}

	private String addWhere(String hql, ChildrenForm form, Map<String, Object> params) {
		if (null != form) {
			/*if (form.getTruename() != null && !"".equals(form.getTruename())) {
				hql += " and t.name like :name";
				params.put("name", "%%" + form.getTruename() + "%%");
			}
			if (form.getAccount() != null && !"".equals(form.getAccount())) {
				hql += " and t.account like :account";
				params.put("account", "%%" + form.getAccount() + "%%");
			}
			if (form.getSex() != null && !"".equals(form.getSex())) {
				hql += " and t.sex like :sex";
				params.put("sex", "%%" + form.getSex() + "%%");
			}
			if (form.getTel() != null && !"".equals(form.getTel())) {
				hql += " and t.tel like :tel";
				params.put("tel", "%%" + form.getTel() + "%%");
			}
			if (form.getEmail() != null && !"".equals(form.getEmail())) {
				hql += " and t.email like :email";
				params.put("email", "%%" + form.getEmail() + "%%");
			}
			if (form.getOrgname() != null && !"".equals(form.getOrgname())) {
				hql += " and t.orgname like :orgname";
				params.put("orgname", "%%" + form.getOrgname() + "%%");
			}
			if (form.getOnlineState() != null && !"".equals(form.getOrgname())) {
				hql += " and t.onlineState=:onlineState";
				params.put("onlineState", form.getOnlineState());
			}*/
		}
		return hql;
	}

}
