package com.galaxy.fireworm.service;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.web.form.ChildrenForm;

public interface ChildrenServiceI {
	
	public Json save(ChildrenForm form) ;
	
	public Json delete(ChildrenForm form) ;

	public Json edit(ChildrenForm form) ;
	
	public ChildrenForm get(ChildrenForm form) ;
	
	public DataGrid datagrid(ChildrenForm form) ;
	
}
