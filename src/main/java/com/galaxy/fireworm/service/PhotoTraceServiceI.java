package com.galaxy.fireworm.service;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.web.form.PhotoTraceForm;

public interface PhotoTraceServiceI {
	
	public Json save(PhotoTraceForm form) ;
	
	public Json delete(PhotoTraceForm form) ;

	public Json edit(PhotoTraceForm form) ;
	
	public PhotoTraceForm get(PhotoTraceForm form) ;
	
	public DataGrid datagrid(PhotoTraceForm form) ;
	
}
