package com.galaxy.fireworm.service;

import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.web.form.PatriarchForm;

public interface PatriarchServiceI {
	
	public Json save(PatriarchForm form) ;
	
	public Json delete(PatriarchForm form) ;

	public Json edit(PatriarchForm form) ;
	
	public PatriarchForm get(PatriarchForm form) ;
	
	public DataGrid datagrid(PatriarchForm form) ;
	
}
