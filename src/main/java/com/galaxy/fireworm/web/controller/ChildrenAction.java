package com.galaxy.fireworm.web.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.image.gm.GMImageUtil;
import com.galaxy.common.util.Constants;
import com.galaxy.common.util.string.StringUtil;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.service.ChildrenServiceI;
import com.galaxy.fireworm.web.form.ChildrenForm;

@Controller
@RequestMapping("/fireworm/childrenAction")
public class ChildrenAction extends BaseController {
	
	@Autowired
	private ChildrenServiceI childrenService ;
	
	@Autowired
	private GMImageUtil gm ;
	
	@RequestMapping("/children_main.do")
	public String children_main(){
		return  Constants.FIREWORM + "children_main" ;
	}
	
	@RequestMapping("/children_form.do")
	public String children_form(ChildrenForm form, Model model){
		if(null != form.getId() && !"".equals(form.getId())) {
			model.addAttribute("id", form.getId()) ;
		}
		return Constants.FIREWORM + "children_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public ChildrenForm get(ChildrenForm form, HttpServletRequest request){
		return this.childrenService.get(form) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	synchronized public Json add(ChildrenForm form){
		return this.childrenService.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(ChildrenForm form){
		return this.childrenService.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(ChildrenForm form){
		return this.childrenService.delete(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(ChildrenForm form){
		return this.childrenService.datagrid(form) ;
	}
	
	/**
	 * 方法描述 :图片压缩 
	 * 创建时间： 2014-5-8 下午5:45:46
	 * 创建者：杨浩泉 
	 * 版本： v1.0
	 * @param form
	 * @return Json
	 */
	@RequestMapping("/doNotNeedAuth_resize_photo.do")
	@ResponseBody
	public Json doNotNeedAuth_resize_photo(ChildrenForm form){
		Json j = new Json() ;
		
		try {
			String srcPath = StringUtil.u2fPath(form.getPhotos_path()) ;
			String fullPath = FilenameUtils.getFullPath(srcPath) ;
			String name = FilenameUtils.getName(srcPath) ;
			String baseName = FilenameUtils.getBaseName(name) ;
			String extension = FilenameUtils.getExtension(name) ;
			
			String resize1 = "80x60"+File.separator+baseName+"."+extension ;
			String resize2 = "150x100"+File.separator+baseName+"."+extension ;
			String resize3 = "170x120"+File.separator+baseName+"."+extension ;
			
			this.gm.compress(srcPath, fullPath+name) ;
			this.gm.resize(srcPath, fullPath+resize1, 80, 60) ;
			this.gm.resize(srcPath, fullPath+resize2, 150, 100) ;
			this.gm.resize(srcPath, fullPath+resize3, 170, 120) ;
			
			String[] imgs = new String[]{
					StringUtil.f2uPath(form.getWeb_root()+name),
					StringUtil.f2uPath(form.getWeb_root()+resize1),
					StringUtil.f2uPath(form.getWeb_root()+resize2),
					StringUtil.f2uPath(form.getWeb_root()+resize3)
			}; 
			
			j.setStatus(true) ;
			j.setObj(imgs) ;
		} catch (Exception e) {
			j.setMsg("文件上传失败！") ;
		}
		return j;
	}
	
	

}
