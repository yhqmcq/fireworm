package com.galaxy.fireworm.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.service.PatriarchServiceI;
import com.galaxy.fireworm.web.form.PatriarchForm;

@Controller
@RequestMapping("/fireworm/patriarchAction")
public class PatriarchAction extends BaseController {
	
	@Autowired
	private PatriarchServiceI patriarchService ;
	
	@RequestMapping("/patriarch_main.do")
	public String patriarch_main(){
		return  Constants.FIREWORM + "patriarch_main" ;
	}
	
	@RequestMapping("/patriarch_form.do")
	public String patriarch_form(PatriarchForm form, Model model){
		if(null != form.getId() && !"".equals(form.getId())) {
			model.addAttribute("id", form.getId()) ;
		}
		return Constants.FIREWORM + "patriarch_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public PatriarchForm get(PatriarchForm form, HttpServletRequest request){
		return this.patriarchService.get(form) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	synchronized public Json add(PatriarchForm form){
		return this.patriarchService.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(PatriarchForm form){
		return this.patriarchService.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(PatriarchForm form){
		return this.patriarchService.delete(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(PatriarchForm form){
		return this.patriarchService.datagrid(form) ;
	}
	

}
