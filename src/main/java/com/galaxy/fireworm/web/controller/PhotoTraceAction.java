package com.galaxy.fireworm.web.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.galaxy.common.image.gm.GMImageUtil;
import com.galaxy.common.util.Constants;
import com.galaxy.common.util.string.StringUtil;
import com.galaxy.common.web.BaseController;
import com.galaxy.common.web.page.DataGrid;
import com.galaxy.common.web.page.Json;
import com.galaxy.fireworm.service.PhotoTraceServiceI;
import com.galaxy.fireworm.web.form.PhotoTraceForm;

@Controller
@RequestMapping("/fireworm/photoTraceAction")
public class PhotoTraceAction extends BaseController {
	
	@Autowired
	private PhotoTraceServiceI phototraceService ;
	
	@Autowired
	private GMImageUtil gm ;
	
	@RequestMapping("/photo_trace_main.do")
	public String photo_trace_main(){
		return  Constants.FIREWORM + "photo_trace_main" ;
	}
	
	@RequestMapping("/photo_trace_form.do")
	public String photo_trace_form(PhotoTraceForm form, Model model){
		if(null != form.getId() && !"".equals(form.getId())) {
			model.addAttribute("id", form.getId()) ;
		}
		return Constants.FIREWORM + "photo_trace_form" ;
	}
	
	@RequestMapping("/get.do")
	@ResponseBody
	public PhotoTraceForm get(PhotoTraceForm form, HttpServletRequest request){
		return this.phototraceService.get(form) ;
	}
	
	@RequestMapping("/add.do")
	@ResponseBody
	synchronized public Json add(PhotoTraceForm form){
		return this.phototraceService.save(form) ;
	}
	
	@RequestMapping("/edit.do")
	@ResponseBody
	public Json edit(PhotoTraceForm form){
		return this.phototraceService.edit(form) ;
	}
	
	@RequestMapping("/delete.do")
	@ResponseBody
	public Json delete(PhotoTraceForm form){
		return this.phototraceService.delete(form) ;
	}
	
	@RequestMapping("/datagrid.do")
	@ResponseBody
	public DataGrid datagrid(PhotoTraceForm form){
		return this.phototraceService.datagrid(form) ;
	}
	
	/**
	 * 方法描述 :图片压缩 
	 * 创建时间： 2014-5-8 下午5:45:46
	 * 创建者：杨浩泉 
	 * 版本： v1.0
	 * @param form
	 * @return Json
	 */
	@RequestMapping("/doNotNeedAuth_resize_photo.do")
	@ResponseBody
	public Json doNotNeedAuth_resize_photo(PhotoTraceForm form){
		Json j = new Json() ;
		
		try {
			String srcPath = StringUtil.u2fPath(form.getPhotos_path()) ;
			String fullPath = FilenameUtils.getFullPath(srcPath) ;
			String name = FilenameUtils.getName(srcPath) ;
			String baseName = FilenameUtils.getBaseName(name) ;
			String extension = FilenameUtils.getExtension(name) ;
			
			String resize1 = "80x60"+File.separator+baseName+"."+extension ;
			String resize2 = "150x100"+File.separator+baseName+"."+extension ;
			String resize3 = "170x120"+File.separator+baseName+"."+extension ;
			
			this.gm.compress(srcPath, fullPath+name) ;
			this.gm.resize(srcPath, fullPath+resize1, 80, 60) ;
			this.gm.resize(srcPath, fullPath+resize2, 150, 100) ;
			this.gm.resize(srcPath, fullPath+resize3, 170, 120) ;
			
			StringBuffer strBuf = new StringBuffer() ;
			strBuf.append(StringUtil.f2uPath(form.getPhoto_dir()+File.separator+name)).append(",") ;
			strBuf.append(StringUtil.f2uPath(form.getPhoto_dir()+File.separator+"80x60"+File.separator+name)).append(",") ;
			strBuf.append(StringUtil.f2uPath(form.getPhoto_dir()+File.separator+"150x100"+File.separator+name)).append(",") ;
			strBuf.append(StringUtil.f2uPath(form.getPhoto_dir()+File.separator+"170x120"+File.separator+name)) ;
			
			PhotoTraceForm f = new PhotoTraceForm() ;
			f.setName(form.getName()) ;
			f.setMobile(form.getMobile()) ;
			f.setPhotos_path(strBuf.toString()) ;
			f.setPhoto_origin("PC") ;
			this.phototraceService.save(f) ;
			
			j.setStatus(true) ;
		} catch (Exception e) {
			e.printStackTrace() ;
			j.setMsg("文件上传失败！") ;
		}
		return j;
	}

}
