package com.galaxy.fireworm.web.form;

import java.util.Date;

import com.galaxy.common.web.page.PageHelper;

public class PhotoTraceForm extends PageHelper {

	private String id ;
	
	private String name ;			//图片来源者姓名
	
	private String mobile ;			//图片来源者联系电话
	
	private String photo_origin ;	//图片来源（PC、Mobile）
	
	private String photo_locale ;	//图片拍摄地点
	
	private String photos_path ;	//图片地址
	
	private String photo_dir ;		//图片存放目录s
	
	private String web_root ;		//图片的URL路径之包含目录
	
	private Date created ;

	
	public String getPhoto_dir() {
		return photo_dir;
	}

	public void setPhoto_dir(String photo_dir) {
		this.photo_dir = photo_dir;
	}

	public String getWeb_root() {
		return web_root;
	}

	public void setWeb_root(String web_root) {
		this.web_root = web_root;
	}


	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhotos_path() {
		return photos_path;
	}

	public void setPhotos_path(String photos_path) {
		this.photos_path = photos_path;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto_origin() {
		return photo_origin;
	}

	public void setPhoto_origin(String photo_origin) {
		this.photo_origin = photo_origin;
	}

	public String getPhoto_locale() {
		return photo_locale;
	}

	public void setPhoto_locale(String photo_locale) {
		this.photo_locale = photo_locale;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	
	
}
