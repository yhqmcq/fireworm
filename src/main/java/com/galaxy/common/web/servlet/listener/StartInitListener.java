package com.galaxy.common.web.servlet.listener;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.galaxy.common.util.Constants;
import com.galaxy.common.web.page.LoginInfoSession;
import com.galaxy.system.service.ModuleServiceI;
import com.galaxy.system.service.UserServiceI;
import com.galaxy.system.web.form.ModuleForm;
import com.galaxy.system.web.form.UserForm;

/**
 * 服务器启动初始化
 * @author Administrator
 *
 */
@WebListener
public class StartInitListener implements ServletContextListener, HttpSessionListener, HttpSessionAttributeListener {
	
	private Logger logger = Logger.getLogger(StartInitListener.class) ;

	private static ApplicationContext ctx = null;
	
	@Override
	public void contextInitialized(ServletContextEvent evt) {
		logger.info("#######[服务器启动]######");
		ctx = WebApplicationContextUtils.getWebApplicationContext(evt.getServletContext());
		
		initialized(evt);
	}

	@Override
	public void contextDestroyed(ServletContextEvent evt) {
		logger.info("#######[服务器关闭]######");
	}
	
	public void initialized(ServletContextEvent evt) {
		initSuperAdmin(evt) ;
		generateMenus(evt) ;
		createParentFolder(evt) ;
	}
	
	/**
	 * 创建站点目录和文件管理目录
	 * @param evt
	 */
	public void createParentFolder(ServletContextEvent evt) {
		String parentPath = new File(evt.getServletContext().getRealPath("/")).getParent() ;
		
		logger.info("#######[启动初始化]######当前项目环境的父目录创建["+Constants.WWWROOT_RELAESE+"]目录") ;
		File wwwroot = new File(parentPath+File.separator+Constants.WWWROOT_RELAESE) ;
		if(!wwwroot.exists()) {
			wwwroot.mkdirs() ;
		}
		logger.info("#######[启动初始化]######当前项目环境的父目录创建["+Constants.FILE_ROOT+"]目录") ;
		File fileroot = new File(parentPath+File.separator+Constants.FILE_ROOT) ;
		if(!fileroot.exists()) {
			fileroot.mkdirs() ;
		}
	}
	
	
	/**
	 * 生成JSON 系统导航菜单
	 * 方法描述 : 
	 * 创建者：杨浩泉 
	 * 项目名称： destiny-cms
	 * 类名： InitServlet.java
	 * 版本： v1.0
	 * 创建时间： 2013-12-23 下午3:11:06 void
	 */
	public void generateMenus(ServletContextEvent evt) {
		logger.info("#######[启动初始化]######生成系统导航菜单") ;
		ModuleServiceI ms = (ModuleServiceI) ctx.getBean("moduleServiceImpl");
		try {
			ms.exportMenusAll(evt.getServletContext()) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sessionCreated(HttpSessionEvent arg0) { }

	/**
	 * 向session里增加属性时调用(用户成功登陆后会调用)
	 */
	@Override
	public void attributeAdded(HttpSessionBindingEvent evt) {
		try {
			//设置会话超时时间
			HttpSession session = evt.getSession();
			session.setMaxInactiveInterval(1200) ;
			
			String keyName = evt.getName();
			if (Constants.SESSION_INFO_NAME.equals(keyName)) {// 如果存入的属性是sessionInfo的话
				LoginInfoSession sessionInfo = (LoginInfoSession) session.getAttribute(keyName);
				if (sessionInfo != null) {
				}
				logger.info("用户登录成功，将用户会话存入Session[Key:"+keyName+" 账号："+sessionInfo.getUser().getAccount()+" 名称："+sessionInfo.getUser().getTruename()+"]");
			}
		} catch (BeansException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * session销毁(用户退出系统时会调用)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent evt) {
		try {
			HttpSession session = evt.getSession();
			if (session != null) {
				
				LoginInfoSession sessionInfo = (LoginInfoSession) session.getAttribute(Constants.SESSION_INFO_NAME);
				if (sessionInfo != null) {
					
					logger.info("用户会话失效，将用户从Session中移除[账号："+sessionInfo.getUser().getAccount()+" 名称："+sessionInfo.getUser().getTruename()+"]");
				}
			}
		} catch (BeansException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent arg0) {
		
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent arg0) {
		
	}
	
	/**
	 * 启动初始化超级管理员账号
	 * @param evt void
	 */
	public void initSuperAdmin(ServletContextEvent evt) {
		logger.info("#######[启动初始化]######启动初始化超级管理员账号") ;
		try {
			UserServiceI userService = (UserServiceI) ctx.getBean("userServiceImpl");
			
			UserForm form = new UserForm() ;
			form.setId("SUP9999") ;
			form.setAccount("admin") ;
			form.setPassword("123456") ;
			form.setStatus(0) ;
			form.setTruename("超级管理员") ;
			form.setEmail("yhqmcq@126.com") ;
			form.setSex("男") ;
			form.setTel("13143536661") ;
			
			userService.addOrUpdate(form) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 启动初始化菜单模块
	 * @param evt void
	 */
	public void initModule(ServletContextEvent evt) {
		logger.info("#######[启动初始化]######启动初始化超级管理员账号") ;
		try {
			ModuleServiceI moduleService = (ModuleServiceI) ctx.getBean("moduleServiceImpl");
			ModuleForm m = new ModuleForm() ;
			m.setId("000001") ;
			m.setModuleName("系统管理") ;
			m.setType("R") ;
			m.setIconCls("icon-standard-application-view-tile") ;
			m.setDisused("Y") ;
			moduleService.saveOrupdate(m) ;
			
			ModuleForm m1 = new ModuleForm() ;
			m1.setId("000002") ;
			m1.setModuleName("模块管理") ;
			m1.setLinkUrl("sysmgr/moduleAction/module_main.do") ;
			m1.setType("F") ;
			m1.setIconCls("icon-standard-application-view-tile") ;
			m1.setDisused("Y") ;
			m1.setPid(m.getId()) ;
			m1.setModuleValue("sys_moudle") ;
			moduleService.saveOrupdate(m1) ; 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
