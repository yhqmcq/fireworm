package com.galaxy.common.util.file;

public class FileInfo {
	
	private String web_root;					//当前项目的上级目录HTTP（TOMCAT WEBAPPS目录）

	private String project_realpath;			//当前项目的绝对路径

	private String webroot_realpath;			//当前项目的上级目录的绝对路径

	private String saveDir;						//文件保存的绝对路径
	
	private String srcName;						//上传到服务器的文件名原名
	
	private String newName;						//最后合并后的新文件名
	
	private String upload_path;					//最后合并后的新文件名

	private String contentType;					//文件类型

	private Long size;							//文件大小
	
	private Integer width = new Integer(0) ;	//像素(宽度)
	
	private Integer height = new Integer(0) ;	//像素(高度)

	public String getWeb_root() {
		return web_root;
	}

	public void setWeb_root(String web_root) {
		this.web_root = web_root;
	}

	public String getProject_realpath() {
		return project_realpath;
	}

	public void setProject_realpath(String project_realpath) {
		this.project_realpath = project_realpath;
	}

	public String getWebroot_realpath() {
		return webroot_realpath;
	}

	public void setWebroot_realpath(String webroot_realpath) {
		this.webroot_realpath = webroot_realpath;
	}

	public String getSaveDir() {
		return saveDir;
	}

	public void setSaveDir(String saveDir) {
		this.saveDir = saveDir;
	}

	public String getSrcName() {
		return srcName;
	}

	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getUpload_path() {
		return upload_path;
	}

	public void setUpload_path(String upload_path) {
		this.upload_path = upload_path;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "FileInfo [web_root=" + web_root + ", project_realpath=" + project_realpath + ", webroot_realpath=" + webroot_realpath + ", saveDir=" + saveDir + ", srcName=" + srcName + ", newName=" + newName + ", upload_path=" + upload_path + ", contentType=" + contentType + ", size=" + size + ", width=" + width + ", height=" + height + "]";
	}

	
}
