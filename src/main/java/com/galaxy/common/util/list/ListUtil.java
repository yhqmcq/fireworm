package com.galaxy.common.util.list;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ListUtil {

	public static List<?> removeDuplicateWithOrder(List<?> list) {
		Set<Object> set = new HashSet<Object>();
		List<Object> newList = new ArrayList<Object>();
		for (Iterator<?> iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (set.add(element))
				newList.add(element);
		}
		return newList;
	}

	/**
	 * 方法描述 :list去重 
	 * 创建时间： 2014-4-29 下午10:41:59
	 * 创建者：杨浩泉 
	 * 版本： v1.0
	 * @param list
	 * @return List
	 */
	public static List removeDuplicate(List list) {
		HashSet h = new HashSet(list);
		list.clear();
		list.addAll(h);
		return list;
	}

}
