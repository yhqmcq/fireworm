/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50522
Source Host           : localhost:3306
Source Database       : fireworm

Target Server Type    : MYSQL
Target Server Version : 50522
File Encoding         : 65001

Date: 2014-05-03 22:20:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `galaxy_system_module`
-- ----------------------------
DROP TABLE IF EXISTS `galaxy_system_module`;
CREATE TABLE `galaxy_system_module` (
  `id` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `disused` varchar(255) DEFAULT NULL,
  `iconCls` varchar(255) DEFAULT NULL,
  `linkUrl` varchar(255) DEFAULT NULL,
  `moduleName` varchar(255) DEFAULT NULL,
  `moduleValue` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `MODULE_PID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_juhx847hp17bm6kpf8p5o0dwm` (`MODULE_PID`),
  CONSTRAINT `FK_juhx847hp17bm6kpf8p5o0dwm` FOREIGN KEY (`MODULE_PID`) REFERENCES `galaxy_system_module` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of galaxy_system_module
-- ----------------------------
INSERT INTO `galaxy_system_module` VALUES ('000001', '2014-04-28 19:03:44', 'Y', 'icon-standard-application-view-tile', null, '系统管理', null, null, '2', null, 'F', '505899');
INSERT INTO `galaxy_system_module` VALUES ('000002', '2014-04-30 15:29:48', 'Y', 'icon-standard-application-view-tile', 'sysmgr/moduleAction/module_main.do', '模块管理', '', null, '1', null, 'F', '000001');
INSERT INTO `galaxy_system_module` VALUES ('029322', '2014-04-28 18:34:06', 'Y', 'icon-standard-application-view-tile', 'sysmgr/filemanager/file_main.do', '文件管理', null, null, '1', null, 'F', null);
INSERT INTO `galaxy_system_module` VALUES ('035229', '2014-05-01 09:26:24', 'Y', 'icon-standard-application-view-tile', 'fireworm/photoTraceAction/photo_trace_main.do', '图片跟踪管理', '', null, '1', null, 'F', '047923');
INSERT INTO `galaxy_system_module` VALUES ('035761', '2014-04-28 19:09:27', 'Y', 'icon-standard-application-view-tile', 'sysmgr/task/task_main.do', '定时作业', null, null, '1', null, 'F', '142443');
INSERT INTO `galaxy_system_module` VALUES ('036574', '2014-04-30 15:05:23', 'Y', 'icon-standard-application-view-tile', 'sysmgr/userAction/user_permit_main.do', '用户授权', '', null, '1', null, 'F', '235809');
INSERT INTO `galaxy_system_module` VALUES ('047923', '2014-04-30 17:31:33', 'Y', 'icon-standard-application-view-tile', '', '寻童网 CMS', '', null, '1', null, 'R', null);
INSERT INTO `galaxy_system_module` VALUES ('082168', '2014-04-30 18:08:44', 'Y', 'icon-standard-application-view-tile', '', '404 公益管理', '', null, '1', null, 'F', '047923');
INSERT INTO `galaxy_system_module` VALUES ('142443', '2014-04-30 15:29:18', 'Y', 'icon-standard-application-view-tile', '', '监控管理', '', null, '3', null, 'F', '505899');
INSERT INTO `galaxy_system_module` VALUES ('202992', '2014-05-01 09:26:42', 'Y', 'icon-standard-application-view-tile', 'fireworm/patriarchAction/patriarch_main.do', '家长信息管理', '', null, '1', null, 'F', '047923');
INSERT INTO `galaxy_system_module` VALUES ('233489', '2014-04-28 18:42:07', 'Y', 'icon-standard-application-view-tile', 'hrmgr/userAction/user_permit_main.do', '用户授权', null, null, '1', null, 'F', null);
INSERT INTO `galaxy_system_module` VALUES ('235809', '2014-04-30 15:28:17', 'Y', 'icon-standard-application-view-tile', '', '权限管理', '', null, '4', null, 'F', '505899');
INSERT INTO `galaxy_system_module` VALUES ('505899', '2014-04-30 15:26:53', 'Y', 'icon-standard-application-view-tile', '', '基础配置管理', '', null, '1', null, 'R', null);
INSERT INTO `galaxy_system_module` VALUES ('573723', '2014-05-02 00:18:52', 'Y', 'icon-standard-application-view-tile', 'fireworm/childrenAction/children_main.do', '儿童信息管理', '', null, '1', null, 'F', '047923');
INSERT INTO `galaxy_system_module` VALUES ('580653', '2014-04-28 18:33:42', 'Y', 'icon-standard-application-view-tile', 'sysmgr/roleAction/role_main.do', '角色管理', null, null, '1', null, 'F', null);
INSERT INTO `galaxy_system_module` VALUES ('597991', '2014-04-28 19:10:09', 'Y', 'icon-standard-application-view-tile', 'sysmgr/filemanager/file_main.do', '文件管理', null, null, '1', null, 'F', '000001');
INSERT INTO `galaxy_system_module` VALUES ('634544', '2014-04-28 19:09:40', 'Y', 'icon-standard-application-view-tile', 'sysmgr/roleAction/role_main.do', '角色管理', null, null, '1', null, 'F', '235809');
INSERT INTO `galaxy_system_module` VALUES ('732260', '2014-04-30 15:05:28', 'Y', 'icon-standard-application-view-tile', 'sysmgr/userAction/user_main.do', '用户管理', '', null, '1', null, 'F', '235809');
INSERT INTO `galaxy_system_module` VALUES ('912535', '2014-04-30 15:07:13', 'Y', 'icon-standard-application-view-tile', 'sysmgr/userAction/user_module_permit.do', '资源权限', '', null, '1', null, 'F', '235809');
